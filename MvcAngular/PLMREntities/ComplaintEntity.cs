﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
namespace PLMREntities
{
    public class ComplaintEntity
    {
        [Required]
        [EmailAddress(ErrorMessage="Invalid Email")]
        public string Email { get; set; }

        
        public string SerialNo { get; set; }
        public string IssueType { get; set; }
        
        [Required]        
        public string TelephoneNo { get; set; }
        [Required]        
        public string ContactPerson { get; set; }
        public string IPAdds { get; set; }
        public string Status { get; set; }
    }
}
