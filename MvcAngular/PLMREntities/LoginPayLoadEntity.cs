﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLMREntities
{
    public class LoginPayLoadEntity
    {
        public string MessageType { get; set; }
        public string IMEI { get; set; }
        public string EmpID { get; set; }

        public string IPAdds { get; set; }
        public string GeoLong { get; set; }
        public string GeoLat { get; set; }
    }
}
