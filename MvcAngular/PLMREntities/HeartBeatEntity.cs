﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLMREntities
{
    public class HeartBeatEntity
    {
        public string MessageType { get; set; }
        public string MessageValue { get; set; }
        public string UserSessionKey { get; set; }
        public string GeoLat { get; set; }
        public string GeoLong { get; set; }
    }
    public class UserActivityEntity
    {
        public string SessionKey { get; set; }
        public int EmployeeMasterID { get; set; }
        public string Activity { get; set; }
        public string GeoLat { get; set; }
        public string GeoLong { get; set; }
        public int ReferenceID { get; set; }
    }
}
