﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLMREntities
{
    public class ContactUsEntity
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string IPAdds { get; set; }        
        public string TelephoneNo { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
        public string Status { get; set; }
    }
}
