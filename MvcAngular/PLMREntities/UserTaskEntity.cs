﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLMREntities
{
    public class UserTaskEntity
    {
        public string UserSessionKey { get; set; }
        public string MessageType { get; set; }
        public int TaskID { get; set; }
        public string TaskDescription { get; set; }
        public string Remarks { get; set; }
        public int StatusID { get; set; }
        public string StatusName { get; set; }
        public DateTime ExpiryDate { get; set; }
        public string GeoLong { get; set; }
        public string GeoLat { get; set; }

    }
}
