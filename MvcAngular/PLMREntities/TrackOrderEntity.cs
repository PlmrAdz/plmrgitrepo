﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLMREntities
{
    public class TrackOrderEntity
    {
        public int TrackID { get; set; }
        public string OrderCode { get; set; }
        public string Status { get; set; }
        public string Remarks { get; set; }
        public DateTime CreatedDTTM { get; set; }
        public DateTime ExpectedDTTM { get; set; }
    }
}
