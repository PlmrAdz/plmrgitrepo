﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLMREntities
{
    public class UserPayLoadEntity
    {
        public string MessageType { get; set; }
        //after validate populate this
        public string UserSessionKey { get; set; }
        public Dictionary<string, string> Settings { get; set; }
        public List<UserTaskEntity> UserTasks { get; set; }        
    }
}
