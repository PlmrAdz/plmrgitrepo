﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PLMREntities
{
    public class ConfirmationDataEntity
    {
        public string Message { get; set; }
        public string CommandAvailable { get; set; }
        public string SessionKey { get; set; }
    }
}
