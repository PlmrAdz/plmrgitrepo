alter PROCEDURE TT_ValidateSessionKey  
@SessionKey UniqueIdentifier  
AS  
BEGIN  
 SELECT top 1 SessionKey  FROM loginlogs(NOLOCK) WHERE SessionKey = @SessionKey and ExpiryDTTM > Getdate()  
END  
  