USE [DB_104585_plmrapi]
GO

/****** Object:  Table [dbo].[StatusMaster]    Script Date: 5/13/2017 10:53:41 PM ******/
DROP TABLE [dbo].[StatusMaster]
GO

/****** Object:  Table [dbo].[StatusMaster]    Script Date: 5/13/2017 10:53:41 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[StatusMaster](
	[StatusID] [int] IDENTITY(1,1) NOT NULL,
	[StatusName] [varchar](20) NOT NULL,
	[Active] [int] NULL,
 CONSTRAINT [PK_StatusMaster] PRIMARY KEY CLUSTERED 
(
	[StatusID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


