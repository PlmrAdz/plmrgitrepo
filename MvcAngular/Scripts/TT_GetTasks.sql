--select * from [dbo].[TaskMaster]
--select * from [dbo].[EmployeeMaster]
--select * from TaskCondition
--select * from LoginLogs

Create procedure TT_GetTasks
@SessionKey UniqueIdentifier
AS
BEGIN
	SELECT C.TaskID,M.TaskNotes,C.Remarks,C.StatusID,S.StatusName,M.ExpiryDTTM FROM TaskCondition C left join StatusMaster S
	on S.StatusID = C.StatusID left join TaskMaster M on M.TaskID = C.TaskID
	left join EmployeeMaster E on E.EmployeeMasterID = C.AssignedTo
	left join LoginLogs L on L.EmployeeMasterID = E.EmployeeMasterID
	and L.SessionKey = @SessionKey and isnull(L.ExpiryDTTM,Getdate()) < GetDate()
	--and C.AssignedTo = ?
END

