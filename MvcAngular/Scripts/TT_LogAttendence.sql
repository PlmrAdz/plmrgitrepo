Create Procedure TT_LogAttendence
@EmpID varchar(20) = null,
@SessionKey UniqueIdentifier,
@Status Varchar(10),
@GeoLong varchar(20),
@GeoLat varchar(20)
AS
BEGIN
Declare @TempEmpID INT 
declare @TempOrgID INT

select top 1 @TempEmpID = e.EmployeeMasterID, @TempOrgID = e.OrganizationID  from loginlogs l ,employeemaster e where l.EmployeeMasterID = E.EmployeeMasterID 
and l.SessionKey = @SessionKey and l.expiryDTTM < getdate()
IF (@Status = 'SignIn')
begin
INSERT INTO dbo.Attendence(EmployeeMasterID,SignIn,CurrentStatus,OrganizationID,GeoLat,GeoLong,CreatedDTTM)
     VALUES(@TempEmpID,Getdate(),@Status,@TempOrgID,@GeoLat,@GeoLong,Getdate())
end
else
Begin
INSERT INTO dbo.Attendence(EmployeeMasterID,SignOut,CurrentStatus,OrganizationID,GeoLat,GeoLong,CreatedDTTM)
     VALUES(@TempEmpID,Getdate(),@Status,@TempOrgID,@GeoLat,@GeoLong,Getdate())
end

end