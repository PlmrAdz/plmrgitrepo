USE [DB_104585_plmrapi]
GO

IF EXISTS(SELECT 'X' FROM SYSOBJECTS WHERE NAME ='SP_COMPLAINTS' AND XTYPE ='P')
BEGIN
DROP PROCEDURE SP_COMPLAINTS
END
GO

CREATE PROCEDURE SP_COMPLAINTS
@Email VARCHAR(50) = NULL,
@SerialNo VARCHAR(50)=NULL,
@IssueType VARCHAR(20)=NULL,
@TelephoneNo VARCHAR(15)=NULL,
@ContactPerson VARCHAR(20)=NULL,
@IPAdds VARCHAR(20) = NULL,
@Status VARCHAR(10)=NULL
AS
BEGIN
INSERT INTO [dbo].[Complaints]([Email],[SerialNo],[IssueType],[TelephoneNo],[ContactPerson],[IPAdds],[CreatedDTTM],[Status])
     VALUES
           (@Email,@SerialNo,@IssueType,@TelephoneNo,@ContactPerson,@IPAdds,GETDATE(),@Status)
END

Go