alter PROCEDURE TT_UserActivity  
@SessionKey UniqueIdentifier,  
@EmployeeMasterID INT,  
@Activity VARCHAR(50),  
@GeoLat VARCHAR(50),  
@GeoLong VARCHAR(50),
@ReferenceID INT = 0  
AS  
BEGIN  
INSERT INTO dbo.UserActivityLogs(SessionKey,EmployeeMasterID,Activity,GeoLat,GeoLong,CreatedDTTM,ReferenceID)  
     VALUES(@SessionKey,@EmployeeMasterID,@Activity,@GeoLat,@GeoLong,GetDate(),@ReferenceID)  
END  
  