USE [DB_104585_plmrapi]
GO

/****** Object:  Table [dbo].[DesignationMaster]    Script Date: 5/13/2017 11:08:27 PM ******/
DROP TABLE [dbo].[DesignationMaster]
GO

/****** Object:  Table [dbo].[DesignationMaster]    Script Date: 5/13/2017 11:08:27 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[DesignationMaster](
	[DesignationID] [int] IDENTITY(1,1) NOT NULL,
	[DesignationName] [varchar](50) NOT NULL,
	[CreatedDTTM] [datetime] NULL,
 CONSTRAINT [PK_DesignationMaster] PRIMARY KEY CLUSTERED 
(
	[DesignationID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


