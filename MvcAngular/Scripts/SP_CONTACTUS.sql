USE [DB_104585_plmrapi]
GO

IF EXISTS(SELECT 'X' FROM SYSOBJECTS WHERE NAME ='SP_CONTACTUS' AND XTYPE ='P')
BEGIN
DROP PROCEDURE SP_CONTACTUS
END
GO

CREATE PROCEDURE SP_CONTACTUS
@IPAdds VARCHAR(20) = NULL,
@Name VARCHAR(20)=NULL,
@Email VARCHAR(25) = NULL,
@TelephoneNo VARCHAR(15)=NULL,
@Subject VARCHAR(50)=NULL,
@Message VARCHAR(100) = NULL,
@Status VARCHAR(10)= NULL
AS
BEGIN

INSERT INTO [dbo].[ContactUS]([IPAdds],[Name],[Email],[TelephoneNo],[Subject],[Message],[Status],[CreatedDTTM])
     VALUES
           (@IPAdds,@Name,@Email,@TelephoneNo,@Subject,@Message,@Status,GETDATE())
END
Go