CREATE procedure TT_ValidateUser          
@Imeid varchar(50) = null,          
@EmployeeID varchar(50)=null,          
@IPAdds varchar(20) = null,          
@SessionKeyOut UniqueIdentifier output          
AS          
BEGIN          
 IF EXISTS(SELECT 'x' FROM EmployeeMaster (nolock) WHERE IMEID = @Imeid AND EmployeeID= @EmployeeID)          
 BEGIN          
 --Check if already has valid session key then return it.          
 declare @EmployeMasterID INT          
 declare @SessionKey varchar(100)          
 SELECT top 1 @EmployeMasterID = EmployeeMasterID FROM EmployeeMaster (nolock) WHERE IMEID = @Imeid AND EmployeeID= @EmployeeID        
 IF EXISTS(SELECT 'x' FROM LoginLogs (nolock) WHERE IMEID = @Imeid AND EmployeeMasterID= @EmployeMasterID AND isnull(ExpiryDTTM,Getdate()) > GetDate())          
 BEGIN          
     
  SELECT @SessionKey = SessionKey FROM LoginLogs (nolock) WHERE IMEID = @Imeid AND EmployeeMasterID= @EmployeMasterID AND isnull(ExpiryDTTM,Getdate()) > GetDate()           
  SET @SessionKeyOut = @SessionKey        
 END          
 else        
  begin      
  SET @SessionKey = NEWID()          
  SELECT top 1 @EmployeMasterID =EmployeeMasterID FROM EmployeeMaster (nolock) WHERE IMEID = @Imeid and EmployeeID= @EmployeeID          
  INSERT INTO dbo.LoginLogs(IPAddress,EmployeeMasterID,IMEID,SessionKey,ExpiryDTTM,CreatedDTTM)          
  values (@IPAdds,@EmployeMasterID,@Imeid,@SessionKey, DATEADD(dd,1,GetDate()),Getdate())          
 end    
 END           
          
 SET @SessionKeyOut = @SessionKey          
END          
--select * from [dbo].[LoginLogs] 