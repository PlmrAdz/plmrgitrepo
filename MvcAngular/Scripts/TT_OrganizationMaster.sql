/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_OrganizationMaster
	(
	OrganizationID int NOT NULL IDENTITY (1, 1),
	Name varchar(50) NOT NULL,
	ContactDetails varchar(50) NULL,
	Email varchar(50) NULL,
	Status int NULL,
	CreatedDTTM datetime NULL,
	ExpiryDTTM datetime NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_OrganizationMaster SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT dbo.Tmp_OrganizationMaster ON
GO
IF EXISTS(SELECT * FROM dbo.OrganizationMaster)
	 EXEC('INSERT INTO dbo.Tmp_OrganizationMaster (OrganizationID, Name, ContactDetails, Email, Status, CreatedDTTM, ExpiryDTTM)
		SELECT OrganizationID, Name, ContactDetails, Email, Status, CreatedDTTM, ExpiryDTTM FROM dbo.OrganizationMaster WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_OrganizationMaster OFF
GO
ALTER TABLE dbo.Attendence
	DROP CONSTRAINT FK_Attendence_OrganizationMaster
GO
ALTER TABLE dbo.TaskMaster
	DROP CONSTRAINT FK_TaskMaster_OrganizationMaster
GO
DROP TABLE dbo.OrganizationMaster
GO
EXECUTE sp_rename N'dbo.Tmp_OrganizationMaster', N'OrganizationMaster', 'OBJECT' 
GO
ALTER TABLE dbo.OrganizationMaster ADD CONSTRAINT
	PK_OrganizationMaster PRIMARY KEY CLUSTERED 
	(
	OrganizationID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
CREATE UNIQUE NONCLUSTERED INDEX IX_OrganizationMaster ON dbo.OrganizationMaster
	(
	OrganizationID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.TaskMaster ADD CONSTRAINT
	FK_TaskMaster_OrganizationMaster FOREIGN KEY
	(
	OrganizationID
	) REFERENCES dbo.OrganizationMaster
	(
	OrganizationID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.TaskMaster SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Attendence ADD CONSTRAINT
	FK_Attendence_OrganizationMaster FOREIGN KEY
	(
	OrganizationID
	) REFERENCES dbo.OrganizationMaster
	(
	OrganizationID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.Attendence SET (LOCK_ESCALATION = TABLE)
GO
COMMIT