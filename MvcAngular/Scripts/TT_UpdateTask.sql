  
alter procedure TT_UpdateTask  
@TaskID INT,  
@Remarks varchar(1024) =null,  
@SessionKey UniqueIdentifier,  
@EmpMasterID INT,  
@StatusID INT  
as  
begin  
  Declare @TempConditionID INT
IF not exists (select 'x' from TaskCondition nolock where taskid =@TaskID)  
begin  
INSERT INTO dbo.TaskCondition  
           (TaskID,Remarks,AssignedTo,ModifiedBy,StatusID,CreatedDTTM)  
     VALUES  
           (@TaskID,@Remarks,@EmpMasterID,@EmpMasterID,@StatusID,GETDATE())  

		   SET @TempConditionID = @@IDENTITY
end  
else  
begin  
 --Update  
	select @TempConditionID = TaskConditionID from TaskCondition where TaskID = @TaskID  
  Update TaskCondition set Remarks = @Remarks,ModifiedBy=@EmpMasterID,StatusID =@StatusID where TaskConditionID= @TempConditionID  
end  
  
--Always enter new entry in logs  
 INSERT INTO dbo.TaskConditionLogs(TaskConditionID,TaskID,Remarks,StatusID,AssignedTo,ModifiedBy,CreatedDTTM)  
     VALUES(@TempConditionID,@TaskID,@Remarks,@StatusID,@EmpMasterID,@EmpMasterID,GETDATE())  
End