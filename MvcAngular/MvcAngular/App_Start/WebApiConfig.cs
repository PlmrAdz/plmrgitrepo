﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace MvcAngular
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            //config.Routes.MapHttpRoute(
            //    name: "DefaultApi",
            //    routeTemplate: "api/{controller}/{id}",
            //    defaults: new { id = RouteParameter.Optional }
            //);

            config.Routes.MapHttpRoute(
               name: "Action",
               routeTemplate: "api/{controller}/{action}/{id}",
               defaults: new { id = RouteParameter.Optional }
           );
          //  config.Routes.MapHttpRoute(
          //    name: "TrackingRoute",
          //    routeTemplate: "api/{Tracker}/{ValidateCustomer}",
          //    defaults: new { Controllers = "Tracker", Action = "ValidateCustomer" }
          //);

            var appXmlType = config.Formatters.XmlFormatter.SupportedMediaTypes.FirstOrDefault(t => t.MediaType == "application/xml");
            config.Formatters.XmlFormatter.SupportedMediaTypes.Remove(appXmlType);
        }
    }
}
