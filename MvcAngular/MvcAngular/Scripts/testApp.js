﻿(function () {
    //create a module
    var app = angular.module('testApp', ['ngRoute']);
    //create a controller
    app.controller('HomeController', function ($scope) {
        $scope.Message = "Working fine";
    });
})();