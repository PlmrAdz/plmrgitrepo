﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcAngular.Models;
using System.Net;
using PlmrsBal;
using PLMREntities;
using System.Net.Mail;

namespace MvcAngular.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {return View();}

        public ActionResult About()
        {return View("about");}

        public ActionResult LedProducts()
        {   
            return View("LedProducts");
        //Chips
        }

        public ActionResult LedDrivers()
        {
            return View("LedDrivers");
        }

        public ActionResult LedStreetLights()
        { 
            //Street lights -fixtures along with bulbs
            return View("LedStreetLights");
        }

        public ActionResult ComparisionChart()
        {
            return View("ComparisionChart");
        }

        public ActionResult OverView()
        {
            return View("OverView");
        }

        public ActionResult HowToBuy()
        {
            return View("HowToBuy");
        }

        public ActionResult TrackOrders()
        {
            return View("TrackOrders");
        }

        public ActionResult ContactUs()
        {
            return View("ContactUs");
        }

        public ActionResult AboutLED()
        {
            return View("AboutLED");
        }

        public ActionResult TermsUse()
        {
            return View("TermsUse");
        }

        public ActionResult PrivacyPolicy()
        { return View("PrivacyPolicy"); }
        public ActionResult Disclaimer()
        { return View("Disclaimer"); }
        [HttpPost]
        public JsonResult ValidateDetails(string strID)
        {
            int intValid = 0;
            int.TryParse(strID, out intValid);
            if (intValid > 0)
                //To Do later with database
                return Json("Thank you, Please call support with this reference number", JsonRequestBehavior.AllowGet);
            else
                return Json("This is invalid Tracking number,</br>Please contact support team.", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult TrackYourOrders(string OrderNumber)
        {
            List<TrackOrderEntity> lstOut = new List<TrackOrderEntity>();
            if (!string.IsNullOrWhiteSpace(OrderNumber))
            {
                using (BalLayer blHelper = new BalLayer())
                {
                    lstOut = blHelper.TrackYourOrder(OrderNumber);
                }
                sendEmail("PLMR-TrackOrder", string.Format("Order:{0}\n Logged : {1}", OrderNumber, DateTime.Now.ToString()));
            }
            return Json(lstOut, JsonRequestBehavior.DenyGet);
        }

        [HttpPost]
        public JsonResult FileComplaint(ComplaintEntity comp)
        {
            if (comp != null)
            { 
                //Validate or return                
                if (string.IsNullOrWhiteSpace(comp.Email) && string.IsNullOrWhiteSpace(comp.SerialNo) &&
                    string.IsNullOrWhiteSpace(comp.IssueType) && 
                    string.IsNullOrWhiteSpace(comp.TelephoneNo) &&
                    string.IsNullOrWhiteSpace(comp.ContactPerson)) goto fileInvalidComplaint;
            }
            
            using (BalLayer blHelper = new BalLayer())
            {
                comp.IPAdds = Request.UserHostAddress;
                blHelper.LogComplaints(comp);
                sendEmail("PLMR-Complaint", string.Format("Name:{0}\n Email :{1} \n Tel: {2} \n IssueType:{3} \n SerialNo:{4} \n Logged : {5}", comp.ContactPerson, comp.Email, comp.TelephoneNo, comp.IssueType, comp.SerialNo,DateTime.Now.ToString()));
            }
            return Json("Thank you, request received", JsonRequestBehavior.DenyGet);

        fileInvalidComplaint:
            return Json("Invalid data entered", JsonRequestBehavior.DenyGet);
        }
        [HttpPost]
        public JsonResult LogContactUs(ContactUsEntity Conta)
        {
            if (string.IsNullOrWhiteSpace(Conta.Name) && string.IsNullOrWhiteSpace(Conta.Email) &&
                string.IsNullOrWhiteSpace(Conta.TelephoneNo) && string.IsNullOrWhiteSpace(Conta.Subject) && string.IsNullOrWhiteSpace(Conta.Message))
                goto ErrorMessage;

            using (BalLayer blHelper = new BalLayer())
            {
                Conta.IPAdds = Request.UserHostAddress;
                Conta.Status = "Active";
                blHelper.LogContactUS(Conta);
                sendEmail("PLMR-ContactUS", string.Format("Name:{0}\n Email :{1} \n Tel: {2} \n Subject:{3} \n Message:{4}", Conta.Name, Conta.Email, Conta.TelephoneNo, Conta.Subject, Conta.Message));
            }
            return Json("Thank you for contacting us.", JsonRequestBehavior.DenyGet);

            ErrorMessage:
            return Json("Invalid data entered, please correct or contact support team.", JsonRequestBehavior.DenyGet);
        }
        public ActionResult ProductDetails(string id)
        {
            //try
            //{
            //    var v = Request.QueryString[0];
            //    System.Runtime.Remoting.Contexts.Context cc = new System.Runtime.Remoting.Contexts.Context();
            //    int i = cc.ContextID;
            //    if (v.Contains("chp"))
            //        return View("ProductDetails");
            //    else if (v.Contains("cob") || v.Contains("FL") || v.Contains("SL"))
            //        return View("LedStreetLights");
            //    else if (v.Equals("lddriver"))
            //        return View("LedDriverDetails");
            //    else
            //        return View("Error");
            //}
            //catch (Exception ex)
            //{ 
            ////To Log exception
            //    return View("Error");
            //}
            if (id.Equals("lddriver"))
                return View("LedDriverDetails");
                    else
            return View("Error");
        }
        public ActionResult LedCompleteDetails(string id)
        {
            try
            {
                if (id.Contains("chp"))
                    return View("ProductDetails");
                else if (id.Contains("cob") || id.Contains("FL") || id.Contains("SL"))
                    return View("LedStreetLights");
                else if (id.Equals("lddriver"))
                    return View("LedDriverDetails");
                else
                    return View("Error");
            }
            catch (Exception ex)
            {
                //To Log exception
                return View("Error");
            }
        }
        public ActionResult StreetLightDetails(string id)
        {
            if (id.Equals("COB30",StringComparison.OrdinalIgnoreCase))
                return View("Cob30");
            if (id.Equals("COB50", StringComparison.OrdinalIgnoreCase))
                return View("Cob50");
            if (id.Equals("COB100", StringComparison.OrdinalIgnoreCase))
                return View("Cob100");
            if (id.Equals("FL30", StringComparison.OrdinalIgnoreCase))
                return View("FL30");
            if (id.Equals("FL2040", StringComparison.OrdinalIgnoreCase))
                return View("FL2040");
            if (id.Equals("FL100", StringComparison.OrdinalIgnoreCase))
                return View("FL100");
            if (id.Equals("SL0920", StringComparison.OrdinalIgnoreCase))
                return View("SL0920");
            if (id.Equals("SL0930", StringComparison.OrdinalIgnoreCase))
                return View("SL0930");
            if (id.Equals("SL2040", StringComparison.OrdinalIgnoreCase))
                return View("SL2040");
            if (id.Equals("SL3060", StringComparison.OrdinalIgnoreCase))
                return View("SL3060");
            else
                return View("Error");
        }
        public ActionResult Accessories()
        {
            return View("Accessories");
        }
      
        public JsonResult AllStudents()
        {
            List<student> students = new List<student>();
            students.Add(new student() { name = "syed", marks = "100" });
            students.Add(new student() { name = "Cool", marks = "200" });
            return Json(students, JsonRequestBehavior.AllowGet);
        }
        private void sendEmail(string strSubject,string strBody)
        {
            try
            {
                string FromMail = "PLMRNotifier@PlmrLightingSolutions.com";
                string emailTo = "syedshafi1976@gmail.com";
                MailMessage mail = new MailMessage();
                SmtpClient SmtpServer = new SmtpClient("mail.PlmrLightingSolutions.com");
                mail.From = new MailAddress(FromMail);
                mail.To.Add(emailTo);
                mail.Subject = strSubject;
                mail.Body = strBody;
                SmtpServer.Port = 25;
                SmtpServer.Credentials = new System.Net.NetworkCredential("PLMRNotifier@PlmrLightingSolutions.com", "$Plmr123");
                SmtpServer.EnableSsl = false;
                SmtpServer.Send(mail);
            }
            catch { }
        }

    }
    public class student
    {
        public string name { get; set; }
        public string marks { get; set; }
    }
}
