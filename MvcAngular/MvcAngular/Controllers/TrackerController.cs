﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
//using System.Net.Http;
using System.Web.Http;
//using System.Web.Mvc;
using MvcAngular.Models;
using TimeTrackerBal;


namespace MvcAngular.Controllers
{
    public class TrackerController : ApiController
    {
        //http://localhost:10185/api/Tracker/get/22
        // GET api/plmr
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}
        // GET api/plmr
        public IEnumerable<LedProductsViewModel> Get()
        {
            List<LedProductsViewModel> lsLed = new List<LedProductsViewModel>();
            lsLed.Add(new LedProductsViewModel() { L_ID = 100, L_Header = "Led header", L_Title = "Led title", L_ImageName = "chip_on_board.png" });
            lsLed.Add(new LedProductsViewModel() { L_ID = 101, L_Header = "Led card", L_Title = "Led card", L_ImageName = "card_icon.png" });
            lsLed.Add(new LedProductsViewModel() { L_ID = 102, L_Header = "Led NY", L_Title = "Led NY", L_ImageName = "usa.png" });
            return lsLed;
        }

        // GET api/plmr/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/plmr
        public void Post([FromBody]string value)
        {
        }

        // PUT api/plmr/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/plmr/5
        public void Delete(int id)
        {

        }

        [HttpPost]
        public HttpResponseMessage ValidateDetails([FromBody]string strID)
        {
            var vReturn = Request.CreateResponse(HttpStatusCode.Accepted, "Cool Dear");
            return vReturn;
        }

        public List<string> AllStudents()
        {
            List<string> lst = new List<string>();
            lst.Add("one");
            lst.Add("Two");
            lst.Add("Three");
            return lst;
        }

        [HttpPost]
        public List<UserPayLoad> RetrieveTasks(string strSessionkey)
        {
            if (!ValidateSessionKey(strSessionkey))
                return null;
            //validate and get the tasks
            Dictionary<string, string> lstSettings = new Dictionary<string, string>();
            lstSettings.Add("DashBoard", "1");
            lstSettings.Add("Reports", "0");
            lstSettings.Add("CreateTask", "0");

            List<UserTask> lstUserTasks = new List<UserTask>();
            lstUserTasks.Add(new UserTask()
            {
                UserSessionKey = Guid.NewGuid().ToString(),
                MessageType = "Tasks",
                TaskID = 10001,
                TaskDescription = "Contact Airtel,Tirupati Circle",
                TaskStatus = "New"
            });


            List<UserPayLoad> lstReturn = new List<UserPayLoad>();
            //Send UserPayLoad
            UserPayLoad usrReturn = new UserPayLoad()
            {
                MessageType = "Login Success",
                UserSessionKey = Guid.NewGuid().ToString(),
                Settings = lstSettings,
                UserTasks = lstUserTasks,
                TokenMessage = "Tasks"
            };
            lstReturn.Add(usrReturn);
            return lstReturn;
        }

        [HttpPost]
        public ConfirmationData ValidateCustomer(LoginPayLoad custLoad)
        {
            //Validate Loginpayload
            if (custLoad != null)
            {
                if ((custLoad.MessageType == "Login") && (custLoad.IMEI == "12345") && (custLoad.EmpID == "100"))
                    return new ConfirmationData() { Message = "success", CommandAvailable = "RetrieveTasks",SessionKey=Guid.NewGuid().ToString() };
                else
                    return ErrorInformation();
            }
            else
                return ErrorInformation();
        }

        [HttpPost]
        public ConfirmationData PingGeoData(HeartbeatData pingData)
        {
            //validate session key
            //Record into db
            //if error send error message
            //if any tasks then send to push for user task request.
            return new ConfirmationData() { Message = "Received", CommandAvailable = "RetrieveTasks" };    
        }

        private ConfirmationData ErrorInformation()
        {
            return new ConfirmationData() { Message = "Error", CommandAvailable = "Retry" };    
        }

        [HttpPost]
        public ConfirmationData UpdateTasks(UserTask usertasks)
        {
            //Validate usersession key
            //Insert or select or update task into DB based on message type
            //Send Confirmation - TokenMessage 
            if (!ValidateSessionKey(usertasks.UserSessionKey))
                return ErrorInformation();
            else
            { 
                //Logic for update or Insert and return the value.
            }
            return new ConfirmationData() { Message = "Operation on tasks received", CommandAvailable = "PingGeofencedata" };
        }

        private bool ValidateSessionKey(string strKey)
        {
            balLayer blHelper = new balLayer();
            return blHelper.ValidateSessionKey(strKey);
        }
    }

    public class ConfirmationData
    {
        public string Message { get; set; }
        public string CommandAvailable { get; set; }
        public string SessionKey { get; set; }
    }
    public class UserPayLoad
    {
        public string MessageType { get; set; }
        //after validate populate this
        public string UserSessionKey { get; set; }
        public Dictionary<string, string> Settings { get; set; }
        public List<UserTask> UserTasks { get; set; }
        public string TokenMessage { get; set; }//error , valid ....
    }
    public class LoginPayLoad
    {
        public string MessageType { get; set; }
        public string IMEI { get; set; }
        public string EmpID { get; set; }
    }
    public class UserTask
    {
        public string UserSessionKey { get; set; }
        public string MessageType { get; set; } //update; new tasks, select
        public int TaskID { get; set; }
        public string TaskDescription { get; set; }
        public string TaskStatus { get; set; }
    }
    public class HeartbeatData
    {
        public string MessageType { get; set; }
        public string MessageValue { get; set; }
        public string UserSessionKey { get; set; }
        public string GeoLat { get; set; }
        public string GeoLong { get; set; }        
    }
}
