﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcAngular.Models
{
    public class LedProductsViewModel
    {
        public int L_ID { get; set; }
        public string L_Header { get; set; }
        public string L_Title { get; set; }
        public string L_ImageName { get; set; }
    }
}