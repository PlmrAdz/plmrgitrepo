﻿//var AllProductsController = function ($scope) {
//    $scope.models = {
//        helloAngular: 'I work good!'
//    };
//}
function students(Header,Title,Imagepath,ID)
{
    this.Header = Header;
    this.Title = Title;
    this.Imagepath = Imagepath;
    this.ID = ID;
}

var AllProductsController = function ($scope, $http)
{
    $scope.plmrProducts = [
        { Header: 'Chip Model', Title: 'AC Chip on board', ImagePath: '/Content/images/led_products/Chips/ACLed.jpg', ID: '/home/LedCompleteDetails?id=chp' },
        { Header: 'Chip Model', Title: 'AC and DC chip on board', ImagePath: '/Content/images/led_products/Chips/DCLED.jpg', ID: '/home/LedCompleteDetails?id=chp' },
        { Header: 'Chip Model', Title: 'AC and DC chip on board', ImagePath: '/Content/images/led_products/Chips/TyfM13-01L .jpg', ID: '/home/LedCompleteDetails?id=chp' }
    ];

    $scope.StreetLightProducts = [
        { Header: 'Chip on board', Title: 'COB 30', ImagePath: '/Content/images/product_images/COB30.png', ID: '/home/StreetLightDetails?id=cob30' },
        { Header: 'Chip on board', Title: 'COB 50', ImagePath: '/Content/images/product_images/COB50.png', ID: '/home/StreetLightDetails?id=cob50' },
        { Header: 'Chip on board', Title: 'COB 100', ImagePath: '/Content/images/product_images/COB100.png', ID: '/home/StreetLightDetails/cob100' },

        { Header: 'Flood light', Title: 'FL 2040', ImagePath: '/Content/images/product_images/FL2040.png', ID: '/home/StreetLightDetails?id=FL2040' },
        { Header: 'Flood light', Title: 'FL 30', ImagePath: '/Content/images/product_images/FL30.png', ID: '/home/StreetLightDetails?id=FL30' },
        { Header: 'Flood light', Title: 'FL 100', ImagePath: '/Content/images/product_images/FL100.png', ID: '/home/StreetLightDetails?id=FL100' },

        { Header: 'Chip set mini', Title: 'SL0920MINI', ImagePath: '/Content/images/product_images/SL0920MINI_5.png', ID: '/home/StreetLightDetails?id=SL0920' },
        { Header: 'Chip set mini', Title: 'SL0930', ImagePath: '/Content/images/product_images/SL0930.png', ID: '/home/StreetLightDetails?id=SL0930' },
        { Header: 'Chip set mini', Title: 'SL2040', ImagePath: '/Content/images/product_images/SL2040.png', ID: '/home/StreetLightDetails?id=SL2040' },
        { Header: 'Chip set mini', Title: 'SL3060', ImagePath: '/Content/images/product_images/SL3060.png', ID: '/home/StreetLightDetails?id=SL3060' }
    ];
    //$scope.students = $http.get('http://localhost:10185/home/AllStudents').success(function (response) {
    //    $scope.students = response.data;
    //});

    $scope.plmrCircuits = [
       { Header: 'Constant current', Title: 'LED drivers', ImagePath: '/Content/images/product_images/driver.png', ID: '/home/productdetails?id=lddriver' },
       { Header: 'Panic - Emergency alarm', Title: 'Custom circuits', ImagePath: '/Content/images/product_images/drivers/driver2.jpg', ID: '#' },
       { Header: 'High - Low voltage protection circuits', Title: 'Protection circuits', ImagePath: '/Content/images/product_images/drivers/driver5.jpg', ID: '#' },
       { Header: 'Dusk to Dawn', Title: 'Automatic switch', ImagePath: '/Content/images/product_images/drivers/autoswitch.JPG', ID: '#' }

    ];

    $scope.plmrAccessories = [
      { Header: 'Mounting clamps', Title: 'Brackets', ImagePath: '/Content/images/Accessories/Bracket1.png' },
      { Header: 'Wall extension', Title: 'Wall mount extender', ImagePath: '/Content/images/Accessories/Bracket2.png', ID: '#' },
  { Header: 'Multiple suport', Title: 'Multi extender', ImagePath: '/Content/images/Accessories/Bracket3.png', ID: '#' },
  { Header: 'Pole bracket', Title: 'Pipe extender', ImagePath: '/Content/images/Accessories/Bracket4.png', ID: '#' },
  { Header: 'Extra grip', Title: 'Wall mount adapter', ImagePath: '/Content/images/Accessories/Bracket5.png', ID: '#' }
    ];

    $scope.plmrStreeLightsCob30 = [
    {
        Subtitle: 'Chipset model', header: 'COB30',
        Description: 'Die cast aluminum housing 30w led street lights',
        Highlights: [{ HighlightDesc: '30W Power', classType: 'fa fa-power-off' }, { HighlightDesc: 'Motion Sensor', classType: 'fa fa-bath' }, { HighlightDesc: 'Auto Cut Off', classType: 'fa fa-podcast' }], specifications: [{}],
        Sliders: [
            { HeaderImagePath: '/Content/images/product_images/COB30/COB30_1.jpg', ThumbImagePath: '/Content/images/product_images/COB30/thumbs/COB30_1.jpg' },
            { HeaderImagePath: '/Content/images/product_images/COB30/COB30_2.jpg', ThumbImagePath: '/Content/images/product_images/COB30/thumbs/COB30_2.jpg' },
            { HeaderImagePath: '/Content/images/product_images/COB30/COB30_3.jpg', ThumbImagePath: '/Content/images/product_images/COB30/thumbs/COB30_3.jpg' },
            { HeaderImagePath: '/Content/images/product_images/COB30/COB30_4.jpg', ThumbImagePath: '/Content/images/product_images/COB30/thumbs/COB30_4.jpg' },
            { HeaderImagePath: '/Content/images/product_images/COB30/COB30_5.jpg', ThumbImagePath: '/Content/images/product_images/COB30/thumbs/COB30_5.jpg' },
            { HeaderImagePath: '/Content/images/product_images/COB30/COB30_6.jpg', ThumbImagePath: '/Content/images/product_images/COB30/thumbs/COB30_6.jpg' }
        ],
        Specifications1: [
            { SpecKey: 'Type', SpecVal: 'Ac or DC' },
            { SpecKey: 'AC', SpecVal: '220 V' },
            { SpecKey: 'DC', SpecVal: '28-34 V 700 mA' },
            { SpecKey: 'Wattage', SpecVal: '30W' },
            { SpecKey: 'Lm/W', SpecVal: '90-120' },
            { SpecKey: 'TC(K)', SpecVal: '6000 - 6500K' },
            { SpecKey: 'Life', SpecVal: 'expectancy 30,000 to 50,000 Hrs' },
            { SpecKey: 'Dimensions', SpecVal: '400L x 171W x 50H mm' },
            { SpecKey: 'Driver space', SpecVal: '172L x 95W x 30H mm' },
            { SpecKey: 'Pipe Diameter', SpecVal: '40 mm' }
        ],
        Specifications2: [
            { SpecKey: 'Motion Sensor', SpecVal: 'Yes' },
            { SpecKey: 'Survillance Camera', SpecVal: '1080p / 720 p' },
            { SpecKey: 'Low / High voltage protection', SpecVal: 'Yes' },
             { SpecKey: 'Auto On / Off', SpecVal: 'Yes' },
             { SpecKey: 'Emergency Alarm', SpecVal: 'Yes' },
             { SpecKey: 'Auto Cooling', SpecVal: 'No' },
             { SpecKey: 'Data Logger', SpecVal: 'Optional' },
             { SpecKey: 'Fire Alarm', SpecVal: 'Optional' },
             { SpecKey: 'Smoke Alarm', SpecVal: 'Optional' }
        ],
    }
    ];

    $scope.plmrStreeLightsCob50 = [
        {
            Subtitle: 'Chipset model', header: 'COB50',
            Description: 'Die cast aluminum housing 50w led street lights',
            Highlights: [{ HighlightDesc: '50W Power', classType: 'fa fa-power-off' }, { HighlightDesc: 'Motion Sensor', classType: 'fa fa-bath' }, { HighlightDesc: 'Auto Cut Off', classType: 'fa fa-podcast' }], specifications: [{}],
            Specifications1: [
                { SpecKey: 'Type', SpecVal: 'Ac or DC' },
                { SpecKey: 'AC', SpecVal: '220 V' },
                { SpecKey: 'DC', SpecVal: '28-34 V 1500 mA' },
                { SpecKey: 'Wattage', SpecVal: '50W' },
                { SpecKey: 'Lm/W', SpecVal: '90-120' },
                { SpecKey: 'TC(K)', SpecVal: '6000 - 6500K' },
                { SpecKey: 'Life', SpecVal: 'expectancy 30,000 to 50,000 Hrs' },
                { SpecKey: 'Dimensions', SpecVal: '510L x 215W x 70H mm' },
                { SpecKey: 'Driver space', SpecVal: '215L x 80W x 30H mm' },
                { SpecKey: 'Pipe Diameter', SpecVal: '50 mm' }
            ],
            Specifications2: [
                { SpecKey: 'Motion Sensor', SpecVal: 'Yes' },
                { SpecKey: 'Survillance Camera', SpecVal: '1080p / 720 p' },
                { SpecKey: 'Low / High voltage protection', SpecVal: 'Yes' },
                 { SpecKey: 'Auto On / Off', SpecVal: 'Yes' },
                 { SpecKey: 'Emergency Alarm', SpecVal: 'Yes' },
                 { SpecKey: 'Auto Cooling', SpecVal: 'No' },
                 { SpecKey: 'Data Logger', SpecVal: 'Optional' },
                 { SpecKey: 'Fire Alarm', SpecVal: 'Optional' },
                 { SpecKey: 'Smoke Alarm', SpecVal: 'Optional' }
            ],
        }
    ];

    $scope.plmrStreeLightsCob100 = [
       {
           Subtitle: 'Chipset model', header: 'COB100',
           Description: 'Die cast aluminum housing 100w led street lights',
           Highlights: [{ HighlightDesc: '100W Power', classType: 'fa fa-power-off' }, { HighlightDesc: 'Motion Sensor', classType: 'fa fa-bath' }, { HighlightDesc: 'Auto Cut Off', classType: 'fa fa-podcast' }], specifications: [{}],
           Specifications1: [
               { SpecKey: 'Type', SpecVal: 'Ac or DC' },
               { SpecKey: 'AC', SpecVal: '220 V' },
               { SpecKey: 'DC', SpecVal: '28-34 V 3500 mA' },
               { SpecKey: 'Wattage', SpecVal: '100W' },
               { SpecKey: 'Lm/W', SpecVal: '90-120' },
               { SpecKey: 'TC(K)', SpecVal: '6000 - 6500K' },
               { SpecKey: 'Life', SpecVal: 'expectancy 30,000 to 50,000 Hrs' },
               { SpecKey: 'Dimensions', SpecVal: '720L x 275W x 87H mm' },
               { SpecKey: 'Driver space', SpecVal: '280L x 95W x 40H mm' },
               { SpecKey: 'Pipe Diameter', SpecVal: '60 mm' }
           ],
           Specifications2: [
               { SpecKey: 'Motion Sensor', SpecVal: 'Yes' },
               { SpecKey: 'Survillance Camera', SpecVal: '1080p / 720 p' },
               { SpecKey: 'Low / High voltage protection', SpecVal: 'Yes' },
                { SpecKey: 'Auto On / Off', SpecVal: 'Yes' },
                { SpecKey: 'Emergency Alarm', SpecVal: 'Yes' },
                { SpecKey: 'Auto Cooling', SpecVal: 'No' },
                { SpecKey: 'Data Logger', SpecVal: 'Optional' },
                { SpecKey: 'Fire Alarm', SpecVal: 'Optional' },
                { SpecKey: 'Smoke Alarm', SpecVal: 'Optional' }
           ],
       }
    ];

    $scope.plmrStreeLightsFL30 = [
      {
          Subtitle: 'Chipset model', header: 'FL30',
          Description: 'Die cast aluminum housing 30w led street lights',
          Highlights: [{ HighlightDesc: '30W Power', classType: 'fa fa-power-off' }, { HighlightDesc: 'Motion Sensor', classType: 'fa fa-bath' }, { HighlightDesc: 'Auto Cut Off', classType: 'fa fa-podcast' }], specifications: [{}],
          Specifications1: [
              { SpecKey: 'Type', SpecVal: 'Ac or DC' },
              { SpecKey: 'AC', SpecVal: '220 V' },
              { SpecKey: 'DC', SpecVal: '28-34 V 700 mA' },
              { SpecKey: 'Wattage', SpecVal: '30W' },
              { SpecKey: 'Lm/W', SpecVal: '90-120' },
              { SpecKey: 'TC(K)', SpecVal: '6000 - 6500K' },
              { SpecKey: 'Life', SpecVal: 'expectancy 30,000 to 50,000 Hrs' },
              { SpecKey: 'Dimensions', SpecVal: '218L x 180W x 130H mm' },
              { SpecKey: 'Driver space', SpecVal: '160L x 90W x 40H mm.' },
              { SpecKey: 'LED Area', SpecVal: '182L x 142W x 32H mm' }
          ],
          Specifications2: [
              { SpecKey: 'Motion Sensor', SpecVal: 'Yes' },
              { SpecKey: 'Survillance Camera', SpecVal: 'Optional' },
              { SpecKey: 'Camera Resolution', SpecVal: '1080p / 720 p' },
              { SpecKey: 'Low / High voltage protection', SpecVal: 'Yes' },
               { SpecKey: 'Auto On / Off', SpecVal: 'Yes' },
               { SpecKey: 'Emergency Alarm', SpecVal: 'Yes' },
               { SpecKey: 'Auto Cooling', SpecVal: 'No' },
               { SpecKey: 'Data Logger', SpecVal: 'Optional' },
               { SpecKey: 'Fire Alarm', SpecVal: 'Optional' },
               { SpecKey: 'Smoke Alarm', SpecVal: 'Optional' }
          ],
      }
    ];
    $scope.plmrStreeLightsFL2040 = [
      {
          Subtitle: 'Chipset model', header: 'FL2040',
          Description: 'Die cast aluminum housing 20 - 40 w led street lights',
          Highlights: [{ HighlightDesc: '20W -40W Power', classType: 'fa fa-power-off' }, { HighlightDesc: 'Motion Sensor', classType: 'fa fa-bath' }, { HighlightDesc: 'Auto Cut Off', classType: 'fa fa-podcast' }], specifications: [{}],
          Specifications1: [
              { SpecKey: 'Type', SpecVal: 'Ac or DC' },
              { SpecKey: 'AC', SpecVal: '220 V' },
              { SpecKey: 'DC', SpecVal: '28-34 V 1000 mA' },
              { SpecKey: 'Wattage', SpecVal: '20W - 40W' },
              { SpecKey: 'Lm/W', SpecVal: '90-120' },
              { SpecKey: 'TC(K)', SpecVal: '6000 - 6500K' },
              { SpecKey: 'Life', SpecVal: 'expectancy 30,000 Hrs' },
              { SpecKey: 'Dimensions', SpecVal: '182L x 143W x 102H mm' },
              { SpecKey: 'Driver space', SpecVal: '105L x 70W x 34H mm' },
              { SpecKey: 'LED Area', SpecVal: '148L x 110W x 28H mm' }
          ],
          Specifications2: [
              { SpecKey: 'Motion Sensor', SpecVal: 'Yes' },
              { SpecKey: 'Survillance Camera', SpecVal: 'Optional' },
              { SpecKey: 'Camera Resolution', SpecVal: '1080p / 720 p' },
              { SpecKey: 'Low / High voltage protection', SpecVal: 'Yes' },
               { SpecKey: 'Auto On / Off', SpecVal: 'Yes' },
               { SpecKey: 'Emergency Alarm', SpecVal: 'Yes' },
               { SpecKey: 'Auto Cooling', SpecVal: 'No' },
               { SpecKey: 'Data Logger', SpecVal: 'Optional' },
               { SpecKey: 'Fire Alarm', SpecVal: 'Optional' },
               { SpecKey: 'Smoke Alarm', SpecVal: 'Optional' }
          ],
      }
    ];
    $scope.plmrStreeLightsFL100 = [
      {
          Subtitle: 'Chipset model', header: 'FL100',
          Description: 'Die cast aluminum housing 100W led street lights',
          Highlights: [{ HighlightDesc: '100W Power', classType: 'fa fa-power-off' }, { HighlightDesc: 'Motion Sensor', classType: 'fa fa-bath' }, { HighlightDesc: 'Auto Cut Off', classType: 'fa fa-podcast' }], specifications: [{}],
          Specifications1: [
              { SpecKey: 'Type', SpecVal: 'Ac or DC' },
              { SpecKey: 'AC', SpecVal: '220 V' },
              { SpecKey: 'DC', SpecVal: '28-34 V 3000 mA' },
              { SpecKey: 'Wattage', SpecVal: '100W' },
              { SpecKey: 'Lm/W', SpecVal: '90-120' },
              { SpecKey: 'TC(K)', SpecVal: '6000 - 6500K' },
              { SpecKey: 'Life', SpecVal: 'expectancy 30,000 Hrs' },
              { SpecKey: 'Dimensions', SpecVal: '388L x 290W x 155H mm' },
              { SpecKey: 'Driver space', SpecVal: '210L x 140W x 49H mm' },
              { SpecKey: 'LED Area', SpecVal: '302L x 202W x 41H mm' }
          ],
          Specifications2: [
              { SpecKey: 'Motion Sensor', SpecVal: 'Yes' },
              { SpecKey: 'Survillance Camera', SpecVal: 'Optional' },
              { SpecKey: 'Camera Resolution', SpecVal: '1080p / 720 p' },
              { SpecKey: 'Low / High voltage protection', SpecVal: 'Yes' },
               { SpecKey: 'Auto On / Off', SpecVal: 'Yes' },
               { SpecKey: 'Emergency Alarm', SpecVal: 'Yes' },
               { SpecKey: 'Auto Cooling', SpecVal: 'No' },
               { SpecKey: 'Data Logger', SpecVal: 'Optional' },
               { SpecKey: 'Fire Alarm', SpecVal: 'Optional' },
               { SpecKey: 'Smoke Alarm', SpecVal: 'Optional' }
          ],
      }
    ];
    $scope.plmrStreeLightsSL0920 = [
     {
         Subtitle: 'Chipset model', header: 'SL0920',
         Description: 'Die cast aluminum housing 9W - 20W led street lights',
         Highlights: [{ HighlightDesc: '9W - 20W Power', classType: 'fa fa-power-off' }, { HighlightDesc: 'Motion Sensor', classType: 'fa fa-bath' }, { HighlightDesc: 'Auto Cut Off', classType: 'fa fa-podcast' }], specifications: [{}],
         Specifications1: [
             { SpecKey: 'Type', SpecVal: 'Ac or DC' },
             { SpecKey: 'AC', SpecVal: '220 V' },
             { SpecKey: 'DC', SpecVal: '28-34 V 700 mA' },
             { SpecKey: 'Wattage', SpecVal: '9W - 20W' },
             { SpecKey: 'Lm/W', SpecVal: '90-120' },
             { SpecKey: 'TC(K)', SpecVal: '6000 - 6500K' },
             { SpecKey: 'Life', SpecVal: 'expectancy 30,000 Hrs' },
             { SpecKey: 'Dimensions', SpecVal: '330L x 96W x 56H mm' },
             { SpecKey: 'Driver space', SpecVal: '106L x 75W x 52H mm' },
             { SpecKey: 'Pipe Diameter', SpecVal: '40 mm' },
             { SpecKey: 'LED Area', SpecVal: '136L x 75W x 14H mm' }
         ],
         Specifications2: [
             { SpecKey: 'Motion Sensor', SpecVal: 'Yes' },
             { SpecKey: 'Survillance Camera', SpecVal: 'Optional' },
             { SpecKey: 'Camera Resolution', SpecVal: '1080p / 720 p' },
             { SpecKey: 'Low / High voltage protection', SpecVal: 'Yes' },
              { SpecKey: 'Auto On / Off', SpecVal: 'Yes' },
              { SpecKey: 'Emergency Alarm', SpecVal: 'Yes' },
              { SpecKey: 'Auto Cooling', SpecVal: 'No' },
              { SpecKey: 'Data Logger', SpecVal: 'Optional' },
              { SpecKey: 'Fire Alarm', SpecVal: 'Optional' },
              { SpecKey: 'Smoke Alarm', SpecVal: 'Optional' }
         ],
     }
    ];
    $scope.plmrStreeLightsSL0930 = [
     {
         Subtitle: 'Chipset model', header: 'SL0930',
         Description: 'Die cast aluminum housing 9W - 30W led street lights',
         Highlights: [{ HighlightDesc: '9W - 30W Power', classType: 'fa fa-power-off' }, { HighlightDesc: 'Motion Sensor', classType: 'fa fa-bath' }, { HighlightDesc: 'Auto Cut Off', classType: 'fa fa-podcast' }], specifications: [{}],
         Specifications1: [
             { SpecKey: 'Type', SpecVal: 'Ac or DC' },
             { SpecKey: 'AC', SpecVal: '220 V' },
             { SpecKey: 'DC', SpecVal: '28-34 V 1000 mA' },
             { SpecKey: 'Wattage', SpecVal: '9W - 30W' },
             { SpecKey: 'Lm/W', SpecVal: '90-120' },
             { SpecKey: 'TC(K)', SpecVal: '6000 - 6500K' },
             { SpecKey: 'Life', SpecVal: 'expectancy 30,000 Hrs' },
             { SpecKey: 'Dimensions', SpecVal: '265L x 124W x 56H mm' },
             { SpecKey: 'Driver space', SpecVal: '130L x 100W x 10H mm' },
             { SpecKey: 'Pipe Diameter', SpecVal: '40 mm' },
             { SpecKey: 'LED Area', SpecVal: '100L x 71W x 48H mm' }
         ],
         Specifications2: [
             { SpecKey: 'Motion Sensor', SpecVal: 'Yes' },
             { SpecKey: 'Survillance Camera', SpecVal: 'Optional' },
             { SpecKey: 'Camera Resolution', SpecVal: '1080p / 720 p' },
             { SpecKey: 'Low / High voltage protection', SpecVal: 'Yes' },
              { SpecKey: 'Auto On / Off', SpecVal: 'Yes' },
              { SpecKey: 'Emergency Alarm', SpecVal: 'Yes' },
              { SpecKey: 'Auto Cooling', SpecVal: 'No' },
              { SpecKey: 'Data Logger', SpecVal: 'Optional' },
              { SpecKey: 'Fire Alarm', SpecVal: 'Optional' },
              { SpecKey: 'Smoke Alarm', SpecVal: 'Optional' }
         ],
     }
    ];
    $scope.plmrStreeLightsSL2040 = [
     {
         Subtitle: 'Chipset model', header: 'SL2040',
         Description: 'Die cast aluminum housing 20W - 40W led street lights',
         Highlights: [{ HighlightDesc: '20W - 40W Power', classType: 'fa fa-power-off' }, { HighlightDesc: 'Motion Sensor', classType: 'fa fa-bath' }, { HighlightDesc: 'Auto Cut Off', classType: 'fa fa-podcast' }], specifications: [{}],
         Specifications1: [
             { SpecKey: 'Type', SpecVal: 'Ac or DC' },
             { SpecKey: 'AC', SpecVal: '220 V' },
             { SpecKey: 'DC', SpecVal: '28-34 V 1200 mA' },
             { SpecKey: 'Wattage', SpecVal: '20W - 40W' },
             { SpecKey: 'Lm/W', SpecVal: '90-120' },
             { SpecKey: 'TC(K)', SpecVal: '6000 - 6500K' },
             { SpecKey: 'Life', SpecVal: 'expectancy 30,000 Hrs' },
             { SpecKey: 'Dimensions', SpecVal: '346L x 123W x 58H mm' },
             { SpecKey: 'Driver space', SpecVal: '96L x 80W x 54H mm' },
             { SpecKey: 'Pipe Diameter', SpecVal: '40 mm' },
             { SpecKey: 'LED Area', SpecVal: '173L x 96W x 14H mm' }
         ],
         Specifications2: [
             { SpecKey: 'Motion Sensor', SpecVal: 'Yes' },
             { SpecKey: 'Survillance Camera', SpecVal: 'Optional' },
             { SpecKey: 'Camera Resolution', SpecVal: '1080p / 720 p' },
             { SpecKey: 'Low / High voltage protection', SpecVal: 'Yes' },
              { SpecKey: 'Auto On / Off', SpecVal: 'Yes' },
              { SpecKey: 'Emergency Alarm', SpecVal: 'Yes' },
              { SpecKey: 'Auto Cooling', SpecVal: 'No' },
              { SpecKey: 'Data Logger', SpecVal: 'Optional' },
              { SpecKey: 'Fire Alarm', SpecVal: 'Optional' },
              { SpecKey: 'Smoke Alarm', SpecVal: 'Optional' }
         ],
     }
    ];
    $scope.plmrStreeLightsSL3060 = [
     {
         Subtitle: 'Chipset model', header: 'SL3060',
         Description: 'Die cast aluminum housing 30W - 60W led street lights',
         Highlights: [{ HighlightDesc: '30W - 60W Power', classType: 'fa fa-power-off' }, { HighlightDesc: 'Motion Sensor', classType: 'fa fa-bath' }, { HighlightDesc: 'Auto Cut Off', classType: 'fa fa-podcast' }], specifications: [{}],
         Specifications1: [
             { SpecKey: 'Type', SpecVal: 'Ac or DC' },
             { SpecKey: 'AC', SpecVal: '220 V' },
             { SpecKey: 'DC', SpecVal: '28-34 V 1700 mA' },
             { SpecKey: 'Wattage', SpecVal: '30W - 60W' },
             { SpecKey: 'Lm/W', SpecVal: '90-120' },
             { SpecKey: 'TC(K)', SpecVal: '6000 - 6500K' },
             { SpecKey: 'Life', SpecVal: 'expectancy 30,000 Hrs' },
             { SpecKey: 'Dimensions', SpecVal: '300L x 160W x 56H mm' },
             { SpecKey: 'Driver space', SpecVal: '135L x 82W x 48H mm' },
             { SpecKey: 'Pipe Diameter', SpecVal: '40 mm' }
         ],
         Specifications2: [
             { SpecKey: 'LED Area', SpecVal: '154L x 135W x 12H mm' },
             { SpecKey: 'Motion Sensor', SpecVal: 'Yes' },
             { SpecKey: 'Survillance Camera', SpecVal: 'Optional' },
             { SpecKey: 'Camera Resolution', SpecVal: '1080p / 720 p' },
             { SpecKey: 'Low / High voltage protection', SpecVal: 'Yes' },
              { SpecKey: 'Auto On / Off', SpecVal: 'Yes' },
              { SpecKey: 'Emergency Alarm', SpecVal: 'Yes' },
              { SpecKey: 'Auto Cooling', SpecVal: 'No' },
              { SpecKey: 'Data Logger', SpecVal: 'Optional' },
              { SpecKey: 'Fire Alarm', SpecVal: 'Optional' },
              { SpecKey: 'Smoke Alarm', SpecVal: 'Optional' }
         ],
     }
    ];
}
    
    //$http.get('/HomeController/AllStudents').then(function (response) {
    //    $scope.students = response.data
    //});
//}



//var AllCircuitController = function ($scope, $http) {
   

//    //$scope.students = $http.get('http://localhost:10185/home/AllStudents').success(function (response) {
//    //    $scope.students = response.data;
//    //});
//}

//var AccessoriesController = function ($scope, $http) {
//    $scope.plmrAccessories = [
//        { Header: 'Mounting clamps', Title: 'Brackets', ImagePath: '/Content/images/Accessories/Bracket1.png', ID: '#' },
//        { Header: 'Wall extension', Title: 'Wall mount extender', ImagePath: '/Content/images/Accessories/Bracket2.png', ID: '#' },
//    { Header: 'Multiple suport', Title: 'Multi extender', ImagePath: '/Content/images/Accessories/Bracket3.png', ID: '#' },
//    { Header: 'Pole bracket', Title: 'Pipe extender', ImagePath: '/Content/images/Accessories/Bracket4.png', ID: '#' },
//    { Header: 'Extra grip', Title: 'Wall mount adapter', ImagePath: '/Content/images/Accessories/Bracket5.png', ID: '#' }
//    ];
//}

//var StreetLightsController = function ($scope, $http) {
//    $scope.plmrStreeLightsCob30= [
//        {
//            Subtitle: 'Chipset model', header: 'COB30',
//            Description: 'Die cast aluminum housing 30w led street lights',
//            Highlights: [{ HighlightDesc: '30W Power',classType:'fa fa-power-off' }, { HighlightDesc: 'Motion Sensor',classType:'fa fa-bath' }, { HighlightDesc: 'Auto Cut Off',classType:'fa fa-podcast' }], specifications: [{}],
//        Sliders: [
//            { HeaderImagePath: '/Content/images/product_images/COB30/COB30_1.jpg', ThumbImagePath: '/Content/images/product_images/COB30/thumbs/COB30_1.jpg' },
//            { HeaderImagePath: '/Content/images/product_images/COB30/COB30_2.jpg', ThumbImagePath: '/Content/images/product_images/COB30/thumbs/COB30_2.jpg' },
//            { HeaderImagePath: '/Content/images/product_images/COB30/COB30_3.jpg', ThumbImagePath: '/Content/images/product_images/COB30/thumbs/COB30_3.jpg' },
//            { HeaderImagePath: '/Content/images/product_images/COB30/COB30_4.jpg', ThumbImagePath: '/Content/images/product_images/COB30/thumbs/COB30_4.jpg' },
//            { HeaderImagePath: '/Content/images/product_images/COB30/COB30_5.jpg', ThumbImagePath: '/Content/images/product_images/COB30/thumbs/COB30_5.jpg' },
//            { HeaderImagePath: '/Content/images/product_images/COB30/COB30_6.jpg', ThumbImagePath: '/Content/images/product_images/COB30/thumbs/COB30_6.jpg' }
//        ],
//        Specifications1:[
//            {SpecKey:'Type',SpecVal :'Ac or DC'},
//            {SpecKey:'AC',SpecVal :'220 V'},
//            {SpecKey:'DC',SpecVal :'28-34 V 700 mA'},
//            {SpecKey:'Wattage',SpecVal :'30W'},
//            {SpecKey:'Lm/W',SpecVal :'90-120'},
//            {SpecKey:'TC(K)',SpecVal :'6000 - 6500K'},
//            {SpecKey:'Life',SpecVal :'expectancy 50,000 Hrs'},
//            {SpecKey:'Dimensions',SpecVal :'400L x 171W x 50H mm'},
//            {SpecKey:'Driver space',SpecVal :'172L x 95W x 30H mm'},
//            {SpecKey:'Pipe Diameter',SpecVal :'40 mm'}
//        ],
//        Specifications2:[
//            {SpecKey:'Motion Sensor',SpecVal :'Yes'},
//            {SpecKey:'Survillance Camera',SpecVal :'1080p / 720 p'},
//            {SpecKey:'Low / High voltage protection',SpecVal :'Yes'},
//             {SpecKey:'Auto On / Off',SpecVal :'Yes'},
//             {SpecKey:'Emergency Alarm',SpecVal :'Yes'},
//             {SpecKey:'Auto Cooling',SpecVal :'No'},
//             {SpecKey:'Data Logger',SpecVal :'Optional'},
//             {SpecKey:'Fire Alarm',SpecVal :'Optional'},
//             {SpecKey:'Smoke Alarm',SpecVal :'Optional'}
//        ],        
//        }
//    ];

//    $scope.plmrStreeLightsCob50 = [
//        {
//            Subtitle: 'Chipset model', header: 'COB50',
//            Description: 'Die cast aluminum housing 50w led street lights',
//            Highlights: [{ HighlightDesc: '50W Power', classType: 'fa fa-power-off' }, { HighlightDesc: 'Motion Sensor', classType: 'fa fa-bath' }, { HighlightDesc: 'Auto Cut Off', classType: 'fa fa-podcast' }], specifications: [{}],
//            Specifications1: [
//                { SpecKey: 'Type', SpecVal: 'Ac or DC' },
//                { SpecKey: 'AC', SpecVal: '220 V' },
//                { SpecKey: 'DC', SpecVal: '28-34 V 1500 mA' },
//                { SpecKey: 'Wattage', SpecVal: '50W' },
//                { SpecKey: 'Lm/W', SpecVal: '90-120' },
//                { SpecKey: 'TC(K)', SpecVal: '6000 - 6500K' },
//                { SpecKey: 'Life', SpecVal: 'expectancy 50,000 Hrs' },
//                { SpecKey: 'Dimensions', SpecVal: '510L x 215W x 70H mm' },
//                { SpecKey: 'Driver space', SpecVal: '215L x 80W x 30H mm' },
//                { SpecKey: 'Pipe Diameter', SpecVal: '50 mm' }
//            ],
//            Specifications2: [
//                { SpecKey: 'Motion Sensor', SpecVal: 'Yes' },
//                { SpecKey: 'Survillance Camera', SpecVal: '1080p / 720 p' },
//                { SpecKey: 'Low / High voltage protection', SpecVal: 'Yes' },
//                 { SpecKey: 'Auto On / Off', SpecVal: 'Yes' },
//                 { SpecKey: 'Emergency Alarm', SpecVal: 'Yes' },
//                 { SpecKey: 'Auto Cooling', SpecVal: 'No' },
//                 { SpecKey: 'Data Logger', SpecVal: 'Optional' },
//                 { SpecKey: 'Fire Alarm', SpecVal: 'Optional' },
//                 { SpecKey: 'Smoke Alarm', SpecVal: 'Optional' }
//            ],
//        }
//    ];

//    $scope.plmrStreeLightsCob100 = [
//       {
//           Subtitle: 'Chipset model', header: 'COB100',
//           Description: 'Die cast aluminum housing 100w led street lights',
//           Highlights: [{ HighlightDesc: '100W Power', classType: 'fa fa-power-off' }, { HighlightDesc: 'Motion Sensor', classType: 'fa fa-bath' }, { HighlightDesc: 'Auto Cut Off', classType: 'fa fa-podcast' }], specifications: [{}],
//           Specifications1: [
//               { SpecKey: 'Type', SpecVal: 'Ac or DC' },
//               { SpecKey: 'AC', SpecVal: '220 V' },
//               { SpecKey: 'DC', SpecVal: '28-34 V 3500 mA' },
//               { SpecKey: 'Wattage', SpecVal: '100W' },
//               { SpecKey: 'Lm/W', SpecVal: '90-120' },
//               { SpecKey: 'TC(K)', SpecVal: '6000 - 6500K' },
//               { SpecKey: 'Life', SpecVal: 'expectancy 50,000 Hrs' },
//               { SpecKey: 'Dimensions', SpecVal: '720L x 275W x 87H mm' },
//               { SpecKey: 'Driver space', SpecVal: '280L x 95W x 40H mm' },
//               { SpecKey: 'Pipe Diameter', SpecVal: '60 mm' }
//           ],
//           Specifications2: [
//               { SpecKey: 'Motion Sensor', SpecVal: 'Yes' },
//               { SpecKey: 'Survillance Camera', SpecVal: '1080p / 720 p' },
//               { SpecKey: 'Low / High voltage protection', SpecVal: 'Yes' },
//                { SpecKey: 'Auto On / Off', SpecVal: 'Yes' },
//                { SpecKey: 'Emergency Alarm', SpecVal: 'Yes' },
//                { SpecKey: 'Auto Cooling', SpecVal: 'No' },
//                { SpecKey: 'Data Logger', SpecVal: 'Optional' },
//                { SpecKey: 'Fire Alarm', SpecVal: 'Optional' },
//                { SpecKey: 'Smoke Alarm', SpecVal: 'Optional' }
//           ],
//       }
//    ];

//    $scope.plmrStreeLightsFL30 = [
//      {
//          Subtitle: 'Chipset model', header: 'FL30',
//          Description: 'Die cast aluminum housing 30w led street lights',
//          Highlights: [{ HighlightDesc: '30W Power', classType: 'fa fa-power-off' }, { HighlightDesc: 'Motion Sensor', classType: 'fa fa-bath' }, { HighlightDesc: 'Auto Cut Off', classType: 'fa fa-podcast' }], specifications: [{}],
//          Specifications1: [
//              { SpecKey: 'Type', SpecVal: 'Ac or DC' },
//              { SpecKey: 'AC', SpecVal: '220 V' },
//              { SpecKey: 'DC', SpecVal: '28-34 V 700 mA' },
//              { SpecKey: 'Wattage', SpecVal: '30W' },
//              { SpecKey: 'Lm/W', SpecVal: '90-120' },
//              { SpecKey: 'TC(K)', SpecVal: '6000 - 6500K' },
//              { SpecKey: 'Life', SpecVal: 'expectancy 50,000 Hrs' },
//              { SpecKey: 'Dimensions', SpecVal: '218L x 180W x 130H mm' },
//              { SpecKey: 'Driver space', SpecVal: '160L x 90W x 40H mm.' },
//              { SpecKey: 'LED Area', SpecVal: '182L x 142W x 32H mm' }
//          ],
//          Specifications2: [
//              { SpecKey: 'Motion Sensor', SpecVal: 'Yes' },
//              { SpecKey: 'Survillance Camera', SpecVal: 'Optional' },
//              { SpecKey: 'Camera Resolution', SpecVal: '1080p / 720 p' },
//              { SpecKey: 'Low / High voltage protection', SpecVal: 'Yes' },
//               { SpecKey: 'Auto On / Off', SpecVal: 'Yes' },
//               { SpecKey: 'Emergency Alarm', SpecVal: 'Yes' },
//               { SpecKey: 'Auto Cooling', SpecVal: 'No' },
//               { SpecKey: 'Data Logger', SpecVal: 'Optional' },
//               { SpecKey: 'Fire Alarm', SpecVal: 'Optional' },
//               { SpecKey: 'Smoke Alarm', SpecVal: 'Optional' }
//          ],
//      }
//    ];
//    $scope.plmrStreeLightsFL2040 = [
//      {
//          Subtitle: 'Chipset model', header: 'FL2040',
//          Description: 'Die cast aluminum housing 20 - 40 w led street lights',
//          Highlights: [{ HighlightDesc: '20W -40W Power', classType: 'fa fa-power-off' }, { HighlightDesc: 'Motion Sensor', classType: 'fa fa-bath' }, { HighlightDesc: 'Auto Cut Off', classType: 'fa fa-podcast' }], specifications: [{}],
//          Specifications1: [
//              { SpecKey: 'Type', SpecVal: 'Ac or DC' },
//              { SpecKey: 'AC', SpecVal: '220 V' },
//              { SpecKey: 'DC', SpecVal: '28-34 V 1000 mA' },
//              { SpecKey: 'Wattage', SpecVal: '20W - 40W' },
//              { SpecKey: 'Lm/W', SpecVal: '90-120' },
//              { SpecKey: 'TC(K)', SpecVal: '6000 - 6500K' },
//              { SpecKey: 'Life', SpecVal: 'expectancy 50,000 Hrs' },
//              { SpecKey: 'Dimensions', SpecVal: '182L x 143W x 102H mm' },
//              { SpecKey: 'Driver space', SpecVal: '105L x 70W x 34H mm' },
//              { SpecKey: 'LED Area', SpecVal: '148L x 110W x 28H mm' }
//          ],
//          Specifications2: [
//              { SpecKey: 'Motion Sensor', SpecVal: 'Yes' },
//              { SpecKey: 'Survillance Camera', SpecVal: 'Optional' },
//              { SpecKey: 'Camera Resolution', SpecVal: '1080p / 720 p' },
//              { SpecKey: 'Low / High voltage protection', SpecVal: 'Yes' },
//               { SpecKey: 'Auto On / Off', SpecVal: 'Yes' },
//               { SpecKey: 'Emergency Alarm', SpecVal: 'Yes' },
//               { SpecKey: 'Auto Cooling', SpecVal: 'No' },
//               { SpecKey: 'Data Logger', SpecVal: 'Optional' },
//               { SpecKey: 'Fire Alarm', SpecVal: 'Optional' },
//               { SpecKey: 'Smoke Alarm', SpecVal: 'Optional' }
//          ],
//      }
//    ];
//    $scope.plmrStreeLightsFL100 = [
//      {
//          Subtitle: 'Chipset model', header: 'FL100',
//          Description: 'Die cast aluminum housing 100W led street lights',
//          Highlights: [{ HighlightDesc: '100W Power', classType: 'fa fa-power-off' }, { HighlightDesc: 'Motion Sensor', classType: 'fa fa-bath' }, { HighlightDesc: 'Auto Cut Off', classType: 'fa fa-podcast' }], specifications: [{}],
//          Specifications1: [
//              { SpecKey: 'Type', SpecVal: 'Ac or DC' },
//              { SpecKey: 'AC', SpecVal: '220 V' },
//              { SpecKey: 'DC', SpecVal: '28-34 V 3000 mA' },
//              { SpecKey: 'Wattage', SpecVal: '100W' },
//              { SpecKey: 'Lm/W', SpecVal: '90-120' },
//              { SpecKey: 'TC(K)', SpecVal: '6000 - 6500K' },
//              { SpecKey: 'Life', SpecVal: 'expectancy 50,000 Hrs' },
//              { SpecKey: 'Dimensions', SpecVal: '388L x 290W x 155H mm' },
//              { SpecKey: 'Driver space', SpecVal: '210L x 140W x 49H mm' },
//              { SpecKey: 'LED Area', SpecVal: '302L x 202W x 41H mm' }
//          ],
//          Specifications2: [
//              { SpecKey: 'Motion Sensor', SpecVal: 'Yes' },
//              { SpecKey: 'Survillance Camera', SpecVal: 'Optional' },
//              { SpecKey: 'Camera Resolution', SpecVal: '1080p / 720 p' },
//              { SpecKey: 'Low / High voltage protection', SpecVal: 'Yes' },
//               { SpecKey: 'Auto On / Off', SpecVal: 'Yes' },
//               { SpecKey: 'Emergency Alarm', SpecVal: 'Yes' },
//               { SpecKey: 'Auto Cooling', SpecVal: 'No' },
//               { SpecKey: 'Data Logger', SpecVal: 'Optional' },
//               { SpecKey: 'Fire Alarm', SpecVal: 'Optional' },
//               { SpecKey: 'Smoke Alarm', SpecVal: 'Optional' }
//          ],
//      }
//    ];
//    $scope.plmrStreeLightsSL0920 = [
//     {
//         Subtitle: 'Chipset model', header: 'SL0920',
//         Description: 'Die cast aluminum housing 9W - 20W led street lights',
//         Highlights: [{ HighlightDesc: '9W - 20W Power', classType: 'fa fa-power-off' }, { HighlightDesc: 'Motion Sensor', classType: 'fa fa-bath' }, { HighlightDesc: 'Auto Cut Off', classType: 'fa fa-podcast' }], specifications: [{}],
//         Specifications1: [
//             { SpecKey: 'Type', SpecVal: 'Ac or DC' },
//             { SpecKey: 'AC', SpecVal: '220 V' },
//             { SpecKey: 'DC', SpecVal: '28-34 V 700 mA' },
//             { SpecKey: 'Wattage', SpecVal: '9W - 20W' },
//             { SpecKey: 'Lm/W', SpecVal: '90-120' },
//             { SpecKey: 'TC(K)', SpecVal: '6000 - 6500K' },
//             { SpecKey: 'Life', SpecVal: 'expectancy 50,000 Hrs' },
//             { SpecKey: 'Dimensions', SpecVal: '330L x 96W x 56H mm' },
//             { SpecKey: 'Driver space', SpecVal: '106L x 75W x 52H mm' },
//             { SpecKey: 'Pipe Diameter', SpecVal: '40 mm' },
//             { SpecKey: 'LED Area', SpecVal: '136L x 75W x 14H mm' }
//         ],
//         Specifications2: [
//             { SpecKey: 'Motion Sensor', SpecVal: 'Yes' },
//             { SpecKey: 'Survillance Camera', SpecVal: 'Optional' },
//             { SpecKey: 'Camera Resolution', SpecVal: '1080p / 720 p' },
//             { SpecKey: 'Low / High voltage protection', SpecVal: 'Yes' },
//              { SpecKey: 'Auto On / Off', SpecVal: 'Yes' },
//              { SpecKey: 'Emergency Alarm', SpecVal: 'Yes' },
//              { SpecKey: 'Auto Cooling', SpecVal: 'No' },
//              { SpecKey: 'Data Logger', SpecVal: 'Optional' },
//              { SpecKey: 'Fire Alarm', SpecVal: 'Optional' },
//              { SpecKey: 'Smoke Alarm', SpecVal: 'Optional' }
//         ],
//     }
//    ];
//    $scope.plmrStreeLightsSL0930 = [
//     {
//         Subtitle: 'Chipset model', header: 'SL0930',
//         Description: 'Die cast aluminum housing 9W - 30W led street lights',
//         Highlights: [{ HighlightDesc: '9W - 30W Power', classType: 'fa fa-power-off' }, { HighlightDesc: 'Motion Sensor', classType: 'fa fa-bath' }, { HighlightDesc: 'Auto Cut Off', classType: 'fa fa-podcast' }], specifications: [{}],
//         Specifications1: [
//             { SpecKey: 'Type', SpecVal: 'Ac or DC' },
//             { SpecKey: 'AC', SpecVal: '220 V' },
//             { SpecKey: 'DC', SpecVal: '28-34 V 1000 mA' },
//             { SpecKey: 'Wattage', SpecVal: '9W - 30W' },
//             { SpecKey: 'Lm/W', SpecVal: '90-120' },
//             { SpecKey: 'TC(K)', SpecVal: '6000 - 6500K' },
//             { SpecKey: 'Life', SpecVal: 'expectancy 50,000 Hrs' },
//             { SpecKey: 'Dimensions', SpecVal: '265L x 124W x 56H mm' },
//             { SpecKey: 'Driver space', SpecVal: '130L x 100W x 10H mm' },
//             { SpecKey: 'Pipe Diameter', SpecVal: '40 mm' },
//             { SpecKey: 'LED Area', SpecVal: '100L x 71W x 48H mm' }
//         ],
//         Specifications2: [
//             { SpecKey: 'Motion Sensor', SpecVal: 'Yes' },
//             { SpecKey: 'Survillance Camera', SpecVal: 'Optional' },
//             { SpecKey: 'Camera Resolution', SpecVal: '1080p / 720 p' },
//             { SpecKey: 'Low / High voltage protection', SpecVal: 'Yes' },
//              { SpecKey: 'Auto On / Off', SpecVal: 'Yes' },
//              { SpecKey: 'Emergency Alarm', SpecVal: 'Yes' },
//              { SpecKey: 'Auto Cooling', SpecVal: 'No' },
//              { SpecKey: 'Data Logger', SpecVal: 'Optional' },
//              { SpecKey: 'Fire Alarm', SpecVal: 'Optional' },
//              { SpecKey: 'Smoke Alarm', SpecVal: 'Optional' }
//         ],
//     }
//    ];
//    $scope.plmrStreeLightsSL2040 = [
//     {
//         Subtitle: 'Chipset model', header: 'SL2040',
//         Description: 'Die cast aluminum housing 20W - 40W led street lights',
//         Highlights: [{ HighlightDesc: '20W - 40W Power', classType: 'fa fa-power-off' }, { HighlightDesc: 'Motion Sensor', classType: 'fa fa-bath' }, { HighlightDesc: 'Auto Cut Off', classType: 'fa fa-podcast' }], specifications: [{}],
//         Specifications1: [
//             { SpecKey: 'Type', SpecVal: 'Ac or DC' },
//             { SpecKey: 'AC', SpecVal: '220 V' },
//             { SpecKey: 'DC', SpecVal: '28-34 V 1200 mA' },
//             { SpecKey: 'Wattage', SpecVal: '20W - 40W' },
//             { SpecKey: 'Lm/W', SpecVal: '90-120' },
//             { SpecKey: 'TC(K)', SpecVal: '6000 - 6500K' },
//             { SpecKey: 'Life', SpecVal: 'expectancy 50,000 Hrs' },
//             { SpecKey: 'Dimensions', SpecVal: '346L x 123W x 58H mm' },
//             { SpecKey: 'Driver space', SpecVal: '96L x 80W x 54H mm' },
//             { SpecKey: 'Pipe Diameter', SpecVal: '40 mm' },
//             { SpecKey: 'LED Area', SpecVal: '173L x 96W x 14H mm' }
//         ],
//         Specifications2: [
//             { SpecKey: 'Motion Sensor', SpecVal: 'Yes' },
//             { SpecKey: 'Survillance Camera', SpecVal: 'Optional' },
//             { SpecKey: 'Camera Resolution', SpecVal: '1080p / 720 p' },
//             { SpecKey: 'Low / High voltage protection', SpecVal: 'Yes' },
//              { SpecKey: 'Auto On / Off', SpecVal: 'Yes' },
//              { SpecKey: 'Emergency Alarm', SpecVal: 'Yes' },
//              { SpecKey: 'Auto Cooling', SpecVal: 'No' },
//              { SpecKey: 'Data Logger', SpecVal: 'Optional' },
//              { SpecKey: 'Fire Alarm', SpecVal: 'Optional' },
//              { SpecKey: 'Smoke Alarm', SpecVal: 'Optional' }
//         ],
//     }
//    ];
//    $scope.plmrStreeLightsSL3060 = [
//     {
//         Subtitle: 'Chipset model', header: 'SL3060',
//         Description: 'Die cast aluminum housing 30W - 60W led street lights',
//         Highlights: [{ HighlightDesc: '30W - 60W Power', classType: 'fa fa-power-off' }, { HighlightDesc: 'Motion Sensor', classType: 'fa fa-bath' }, { HighlightDesc: 'Auto Cut Off', classType: 'fa fa-podcast' }], specifications: [{}],
//         Specifications1: [
//             { SpecKey: 'Type', SpecVal: 'Ac or DC' },
//             { SpecKey: 'AC', SpecVal: '220 V' },
//             { SpecKey: 'DC', SpecVal: '28-34 V 1700 mA' },
//             { SpecKey: 'Wattage', SpecVal: '30W - 60W' },
//             { SpecKey: 'Lm/W', SpecVal: '90-120' },
//             { SpecKey: 'TC(K)', SpecVal: '6000 - 6500K' },
//             { SpecKey: 'Life', SpecVal: 'expectancy 50,000 Hrs' },
//             { SpecKey: 'Dimensions', SpecVal: '300L x 160W x 56H mm' },
//             { SpecKey: 'Driver space', SpecVal: '135L x 82W x 48H mm' },
//             { SpecKey: 'Pipe Diameter', SpecVal: '40 mm' }             
//         ],
//         Specifications2: [
//             { SpecKey: 'LED Area', SpecVal: '154L x 135W x 12H mm' },
//             { SpecKey: 'Motion Sensor', SpecVal: 'Yes' },
//             { SpecKey: 'Survillance Camera', SpecVal: 'Optional' },
//             { SpecKey: 'Camera Resolution', SpecVal: '1080p / 720 p' },
//             { SpecKey: 'Low / High voltage protection', SpecVal: 'Yes' },
//              { SpecKey: 'Auto On / Off', SpecVal: 'Yes' },
//              { SpecKey: 'Emergency Alarm', SpecVal: 'Yes' },
//              { SpecKey: 'Auto Cooling', SpecVal: 'No' },
//              { SpecKey: 'Data Logger', SpecVal: 'Optional' },
//              { SpecKey: 'Fire Alarm', SpecVal: 'Optional' },
//              { SpecKey: 'Smoke Alarm', SpecVal: 'Optional' }
//         ],
//     }
//    ];
//}

AllProductsController.$inject = ['$scope'];