﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PLMREntities;

namespace PlmrDalLayer
{
    public class SqlDalLayer : BaseDalLayer
    {
        public void LogContactUS(ContactUsEntity conta)
        {
            base.Base_LogContactUS(conta);
        }
        public void LogComplaints(ComplaintEntity Comp)
        {
            base.Base_LogComplaints(Comp);
        }
        public List<TrackOrderEntity> TrackYourOrder(string strOrderCode)
        {
            return base.Base_TrackYourOrder(strOrderCode);
        }

        //public override System.Data.SqlClient.SqlConnection SqlConnection
        //{
        //    get
        //    {
        //        throw new NotImplementedException();
        //    }
        //    set
        //    {
        //        throw new NotImplementedException();
        //    }
        //}
        public bool ValidateSessionKey(string strSessionKey)
        {
            return base.ValidateSessionKey(strSessionKey);
        }
        public List<UserTaskEntity> GetMyTasks(string strSessionKey)
        {
            return base.GetMyTasks(strSessionKey);
        }

        public void LogAttendence(string strSessionKey, LoginPayLoadEntity custLoad)
        {
            base.LogAttendence(strSessionKey, custLoad);
        }
        public Dictionary<string, string> GetUserData(string strSessionKey)
        {
            return base.GetUserData(strSessionKey);
        }

        public int GetEmpMasterID(string strSessionKey)
        {
            return base.GetEmpMasterID(strSessionKey);
        }

        public void LogUserActivity(UserActivityEntity userActivity)
        {
            base.LogUserActivity(userActivity);
        }

        public void UpdateTask(UserTaskEntity usertasks)
        {
            base.UpdateTask(usertasks);
        }
    }
}
