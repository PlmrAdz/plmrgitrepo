﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PLMREntities;
using SQLDatabase.Data;
using Utilities;

namespace PlmrDalLayer
{
    public abstract class BaseDalLayer
    {
        private string ConnString = ConfigurationManager.AppSettings["Conn"];
        public BaseDalLayer()
        {
            //   SqlConnection = new System.Data.SqlClient.SqlConnection();

            if (string.IsNullOrEmpty(ConnString))
                ConnString = ConfigurationManager.AppSettings["Conn"];
        }
       
        public void Base_LogContactUS(ContactUsEntity conta)
        {
            try
            {
                    SqlParameter[] sqlParam = new SqlParameter[7];
                    sqlParam[0] = new SqlParameter("@IPAdds", SqlDbType.VarChar);
                    sqlParam[0].Value = conta.IPAdds;
                    sqlParam[1] = new SqlParameter("@Name", SqlDbType.VarChar);
                    sqlParam[1].Value = conta.Name;
                    sqlParam[2] = new SqlParameter("@Email", SqlDbType.VarChar);
                    sqlParam[2].Value = conta.Email;
                    sqlParam[3] = new SqlParameter("@TelephoneNo", SqlDbType.VarChar);
                    sqlParam[3].Value = conta.TelephoneNo;
                    sqlParam[4] = new SqlParameter("@Subject", SqlDbType.VarChar);
                    sqlParam[4].Value = conta.Subject;
                    sqlParam[5] = new SqlParameter("@Message", SqlDbType.VarChar);
                    sqlParam[5].Value = conta.Message;
                    sqlParam[6] = new SqlParameter("@Status", SqlDbType.VarChar);
                    sqlParam[6].Value = conta.Status;
                    SqlHelper.ExecuteNonQuery(ConnString, CommandType.StoredProcedure, "SP_CONTACTUS", sqlParam);
            }
            catch (Exception ex)
            {
                UtilityHelper.LogToFileExceptions("ContactUS:" + ex.Message.ToString());
                LogExceptions("ContactUS:", ex.Message.ToString());
            }

        }

        public void Base_LogComplaints(ComplaintEntity comp)
        {
            try
            {
                    SqlParameter[] sqlParam = new SqlParameter[7];
                    sqlParam[0] = new SqlParameter("@Email", SqlDbType.VarChar);
                    sqlParam[0].Value = comp.Email;
                    sqlParam[1] = new SqlParameter("@SerialNo", SqlDbType.VarChar);
                    sqlParam[1].Value = comp.SerialNo;
                    sqlParam[2] = new SqlParameter("@IssueType", SqlDbType.VarChar);
                    sqlParam[2].Value = comp.IssueType;
                    sqlParam[3] = new SqlParameter("@TelephoneNo", SqlDbType.VarChar);
                    sqlParam[3].Value = comp.TelephoneNo;
                    sqlParam[4] = new SqlParameter("@ContactPerson", SqlDbType.VarChar);
                    sqlParam[4].Value = comp.ContactPerson;
                    sqlParam[5] = new SqlParameter("@IPAdds", SqlDbType.VarChar);
                    sqlParam[5].Value = comp.IPAdds;
                    sqlParam[6] = new SqlParameter("@Status", SqlDbType.VarChar);
                    sqlParam[6].Value = comp.Status;
                    SqlHelper.ExecuteNonQuery(ConnString, CommandType.StoredProcedure, "SP_COMPLAINTS", sqlParam);
            }
            catch (Exception ex)
            {
                UtilityHelper.LogToFileExceptions("ContactUS:" + ex.Message.ToString());
                LogExceptions("ContactUS:", ex.Message.ToString());
            }
        }

        public List<TrackOrderEntity> Base_TrackYourOrder(string strOrderCode)
        {
            List<TrackOrderEntity> lstReturn = new List<TrackOrderEntity>();
            try
            {
                SqlParameter[] sqlParam = new SqlParameter[1];
                sqlParam[0] = new SqlParameter("@OrderCode", SqlDbType.VarChar);
                sqlParam[0].Value = strOrderCode;
                SqlDataReader sqReader = SqlHelper.ExecuteReader(ConnString, CommandType.StoredProcedure, "SP_TRACKORDER", sqlParam);
                while (sqReader.Read())
                {
                    lstReturn.Add(new TrackOrderEntity()
                    {
                        TrackID = UtilityHelper.FormatInt(sqReader["TrackID"]),
                        OrderCode = UtilityHelper.FormatString(sqReader["OrderCode"]),
                        Status = UtilityHelper.FormatString(sqReader["Status"]),
                        Remarks = UtilityHelper.FormatString(sqReader["Remarks"]),
                        CreatedDTTM = UtilityHelper.FormatDateTime(sqReader["CreatedDTTM"]),
                        ExpectedDTTM = UtilityHelper.FormatDateTime(sqReader["ExpectedDTTM"])
                    });
                }
            }
            catch (Exception ex)
            {
                UtilityHelper.LogToFileExceptions("ContactUS:" + ex.Message.ToString());
                LogExceptions("ContactUS:", ex.Message.ToString());
            }
            return lstReturn;
        }
        #region LogExceptions
        /// <summary>
        /// 
        /// </summary>
        /// <param name="strMethoName"></param>
        /// <param name="strErrorMessage"></param>
        internal void LogExceptions(string strMethoName, string strErrorMessage)
        {
                SqlParameter[] sqlParam = new SqlParameter[2];
                sqlParam[0] = new SqlParameter("@MethodName", SqlDbType.VarChar);
                sqlParam[0].Value = strMethoName;
                sqlParam[1] = new SqlParameter("@Remarks", SqlDbType.VarChar);
                sqlParam[1].Value = strErrorMessage;
                SqlHelper.ExecuteNonQuery(ConnString, CommandType.StoredProcedure, "SP_LOGEXCEPTIONS", sqlParam);            
        }
        #endregion



        internal bool ValidateSessionKey(string strSessionKey)
        {
            bool blnReturn = false;
            SqlParameter[] sqlParam = new SqlParameter[1];
            sqlParam[0] = new SqlParameter("@SessionKey", SqlDbType.VarChar);
            sqlParam[0].Value = strSessionKey;
            var sqlVReader = SqlHelper.ExecuteReader(ConnString, CommandType.StoredProcedure, "TT_ValidateSessionKey",sqlParam);
            while (sqlVReader.Read())
            {
                if (!string.IsNullOrWhiteSpace(UtilityHelper.FormatString(sqlVReader["SessionKey"])))
                    blnReturn = true;
            }
            return blnReturn;
        }
        internal List<UserTaskEntity> GetMyTasks(string strSessionKey)
        {
            List<UserTaskEntity> lstReturn = new List<UserTaskEntity>();
            SqlParameter[] sqlParam = new SqlParameter[1];
            sqlParam[0] = new SqlParameter("@SessionKey", SqlDbType.VarChar);
            sqlParam[0].Value = strSessionKey;
            var sqlVReader = SqlHelper.ExecuteReader(ConnString, CommandType.StoredProcedure, "TT_GetTasks",sqlParam);
            while (sqlVReader.Read())
            {
                lstReturn.Add(new UserTaskEntity()
                {
                    TaskID = UtilityHelper.FormatInt(sqlVReader["TaskID"]),
                    TaskDescription = UtilityHelper.FormatString(sqlVReader["TaskNotes"]),
                    Remarks = UtilityHelper.FormatString(sqlVReader["Remarks"]),
                    StatusID = UtilityHelper.FormatInt(sqlVReader["StatusID"]),
                    StatusName = UtilityHelper.FormatString(sqlVReader["StatusName"]),
                    ExpiryDate = UtilityHelper.FormatDateTime(sqlVReader["ExpiryDTTM"])

                });

            }
            return lstReturn;
        }
        public string ValidateUser(LoginPayLoadEntity payLoads)
        {
            string strReturn = string.Empty;
            SqlParameter[] sqlParam = new SqlParameter[4];
            sqlParam[0] = new SqlParameter("@Imeid", SqlDbType.VarChar);
            sqlParam[0].Value = payLoads.IMEI;
            sqlParam[1] = new SqlParameter("@EmployeeID", SqlDbType.VarChar);
            sqlParam[1].Value = payLoads.EmpID;
            sqlParam[2] = new SqlParameter("@IPAdds", SqlDbType.VarChar);
            sqlParam[2].Value = payLoads.IPAdds;
            sqlParam[3] = new SqlParameter("@SessionKeyOut", SqlDbType.UniqueIdentifier, 250);
            sqlParam[3].Direction = ParameterDirection.Output;
            SqlHelper.ExecuteNonQuery(ConnString, CommandType.StoredProcedure, "TT_ValidateUser", sqlParam);
            strReturn = UtilityHelper.FormatString(sqlParam[3].Value);
            return strReturn;
        }

        internal void LogAttendence(string strSessionKey, LoginPayLoadEntity custLoad)
        {
            string strReturn = string.Empty;
            SqlParameter[] sqlParam = new SqlParameter[5];
            sqlParam[0] = new SqlParameter("@EmpID", SqlDbType.VarChar);
            sqlParam[0].Value = custLoad.EmpID;
            sqlParam[1] = new SqlParameter("@SessionKey ", SqlDbType.VarChar);
            sqlParam[1].Value = strSessionKey;
            sqlParam[2] = new SqlParameter("@Status", SqlDbType.VarChar);
            sqlParam[2].Value = custLoad.MessageType;
            sqlParam[3] = new SqlParameter("@GeoLong", SqlDbType.VarChar);
            sqlParam[3].Value = custLoad.GeoLong;
            sqlParam[4] = new SqlParameter("@GeoLat", SqlDbType.VarChar);
            sqlParam[4].Value = custLoad.GeoLat;

            SqlHelper.ExecuteNonQuery(ConnString, CommandType.StoredProcedure, "TT_LogAttendence", sqlParam);

        }
        internal Dictionary<string, string> GetUserData(string strSessionKey)
        {
            Dictionary<string, string> dicReturn = new Dictionary<string, string>();
                var sqlReader = SqlHelper.ExecuteReader(ConnString, CommandType.Text, string.Format("select * from {0} (nolock)", "LoginLogs"));
                while (sqlReader.Read())
                {
                    //To Do check column and value
                    //dicReturn.Add(sqlReader.col
                }            
            return dicReturn;
        }

        internal int GetEmpMasterID(string strSessionKey)
        {
            int intReturn = 0;
                var sqlReader = SqlHelper.ExecuteReader(ConnString, CommandType.Text, string.Format("select * from {0} (nolock)", "LoginLogs"));
                while (sqlReader.Read())
                {
                   intReturn= UtilityHelper.FormatInt(sqlReader["EmployeeMasterID"]);
                }            
            return intReturn;
        }

        internal void LogUserActivity(UserActivityEntity userActivity)
        {
            SqlParameter[] sqlParam = new SqlParameter[6];
            sqlParam[0] = new SqlParameter("@SessionKey", SqlDbType.UniqueIdentifier);
            sqlParam[0].Value = Guid.Parse(userActivity.SessionKey);
            sqlParam[1] = new SqlParameter("@EmployeeMasterID", SqlDbType.Int);
            sqlParam[1].Value = userActivity.EmployeeMasterID;
            sqlParam[2] = new SqlParameter("@Activity", SqlDbType.VarChar);
            sqlParam[2].Value = userActivity.Activity;
            sqlParam[3] = new SqlParameter("@GeoLat", SqlDbType.VarChar);
            sqlParam[3].Value = userActivity.GeoLat;
            sqlParam[4] = new SqlParameter("@GeoLong", SqlDbType.VarChar);
            sqlParam[4].Value = userActivity.GeoLong;
            sqlParam[5] = new SqlParameter("@ReferenceID", SqlDbType.VarChar);
            sqlParam[5].Value = userActivity.ReferenceID;
            SqlHelper.ExecuteNonQuery(ConnString, CommandType.StoredProcedure, "TT_UserActivity", sqlParam);
        }

        internal void UpdateTask(UserTaskEntity usertasks)
        {
            SqlParameter[] sqlParam = new SqlParameter[5];
            sqlParam[0] = new SqlParameter("@TaskID", SqlDbType.Int);
            sqlParam[0].Value = usertasks.TaskID;
            sqlParam[1] = new SqlParameter("@Remarks", SqlDbType.VarChar);
            sqlParam[1].Value = usertasks.Remarks;
            sqlParam[2] = new SqlParameter("@SessionKey", SqlDbType.UniqueIdentifier);
            sqlParam[2].Value = Guid.Parse(usertasks.UserSessionKey);
            sqlParam[3] = new SqlParameter("@EmpMasterID", SqlDbType.Int);
            sqlParam[3].Value = GetEmpMasterID(usertasks.UserSessionKey);
            sqlParam[4] = new SqlParameter("@StatusID", SqlDbType.Int);
            sqlParam[4].Value = usertasks.StatusID;//statusname is also available.            
            SqlHelper.ExecuteNonQuery(ConnString, CommandType.StoredProcedure, "TT_UpdateTask", sqlParam);
        }
    }

}

