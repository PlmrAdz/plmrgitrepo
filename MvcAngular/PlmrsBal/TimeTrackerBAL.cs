﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PlmrDalLayer;
namespace PlmrsBal
{
    public class TimeTrackerBAL :ITimeTrackerLayer
    {
        public TimeTrackerBAL()
        { 
        
        }

        public List<PLMREntities.UserTaskEntity> GetMyTasks(string strSessionKey)
        {
            SQLBaseManager sqlhelper = new SQLBaseManager();
            List<DbParameter> lstParam = new List<DbParameter>();
            lstParam.Add(new DbParameter(){ Name ="", Direction= System.Data.ParameterDirection.Input, Value =""});
            sqlhelper.ExecuteNonQuery("", lstParam);
        }

        public bool ValidateSessionKey(string strSessionKey)
        {
            throw new NotImplementedException();
        }

        public Dictionary<string, string> RetrieveSettings(string strSessionKey)
        {
            throw new NotImplementedException();
        }

        public void LogUserActivity(PLMREntities.UserActivityEntity userActivity)
        {
            throw new NotImplementedException();
        }
    }
}
