﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PlmrDalLayer;
using PLMREntities;

namespace PlmrsBal
{
    public class TimeTrackerBALLayer : ITimeTrackerLayer
    {
        private SqlDalLayer dlHelper;
        public TimeTrackerBALLayer()
        {
            dlHelper = new SqlDalLayer();
        }
        public List<PLMREntities.UserTaskEntity> GetMyTasks(string strSessionKey)
        {
            return dlHelper.GetMyTasks(strSessionKey);
        }

        public bool ValidateSessionKey(string strSessionKey)
        {
            return dlHelper.ValidateSessionKey(strSessionKey);
        }

        public Dictionary<string, string> RetrieveSettings(string strSessionKey)
        {
            throw new NotImplementedException();
        }

        public void LogUserActivity(UserActivityEntity userActivity)
        {
            dlHelper.LogUserActivity(userActivity);
        }


        public bool ValidateUser()
        {
            throw new NotImplementedException();
        }

        public bool Validateuser()
        {
            throw new NotImplementedException();
        }

        public string Validateuser(LoginPayLoadEntity custLoad)
        {
            return dlHelper.ValidateUser(custLoad);
        }


        public void LogAttendence(string strSessionKey, LoginPayLoadEntity custLoad)
        {
            dlHelper.LogAttendence(strSessionKey, custLoad);
        }
        public Dictionary<string, string> GetUserData(string strSessionKey)
        {
            return dlHelper.GetUserData(strSessionKey);
        }
        public int GetEmpMasterID(string strSessionKey)
        {
            return dlHelper.GetEmpMasterID(strSessionKey);
        }


        public void UpdateTask(UserTaskEntity usertasks)
        {
            dlHelper.UpdateTask(usertasks);
        }
    }
}
