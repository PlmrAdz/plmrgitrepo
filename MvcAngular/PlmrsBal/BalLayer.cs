﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PLMREntities;
using PlmrDalLayer;
namespace PlmrsBal
{
    public class BalLayer : IBalLayer, IDisposable
    {
        private SqlDalLayer dlHelper;
        public BalLayer()
        {
            dlHelper = new SqlDalLayer();
        }
        //public OrdersEntity VerifyYourOrders(string strOrderNumber)
        //{
        //    return dlHelper.VerifyYourOrders(strOrderNumber);
        //}

        public void Dispose()
        {
            //To Do implement dispose
            if (dlHelper != null)
                dlHelper = null;
        }


        public bool ValidateSessionKey(string strSessionKey)
        {
            return dlHelper.ValidateSessionKey(strSessionKey);
        }


        public void LogContactUS(ContactUsEntity conta)
        {
            try
            {
                dlHelper.LogContactUS(conta);
            }
            catch { }
        }

        public void LogComplaints(ComplaintEntity Comp)
        {
            try
            {
                dlHelper.LogComplaints(Comp);
            }
            catch { }//To Do for later
        }

        public List<TrackOrderEntity> TrackYourOrder(string strOrderCode)
        {
            return dlHelper.TrackYourOrder(strOrderCode);
        }
    }
}
