﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PLMREntities;
namespace PlmrsBal
{
    public interface IBalLayer
    {
        //OrdersEntity VerifyYourOrders(string strOrderNumber);
        bool ValidateSessionKey(string strSessionKey);
        void LogContactUS(ContactUsEntity conta);
        void LogComplaints(ComplaintEntity Comp);
        List<TrackOrderEntity> TrackYourOrder(string strOrderCode);
    }

    public interface ITimeTrackerLayer
    {
         List<UserTaskEntity> GetMyTasks(string strSessionKey);
        bool ValidateSessionKey(string strSessionKey);
        Dictionary<string, string> RetrieveSettings(string strSessionKey);
        void LogUserActivity(UserActivityEntity userActivity);
        bool ValidateUser();
        bool Validateuser();

        string Validateuser(LoginPayLoadEntity custLoad);

        void LogAttendence(string strSessionKey, LoginPayLoadEntity custLoad);
        Dictionary<string, string> GetUserData(string strSessionKey);
        int GetEmpMasterID(string strSessionKey);

        void UpdateTask(UserTaskEntity usertasks);
    }
}
