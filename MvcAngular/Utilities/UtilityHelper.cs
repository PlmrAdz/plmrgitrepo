﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilities
{
    public static class UtilityHelper
    {
        public static void LogToFileExceptions(string strMsg)
        { 
        //To Do
        }
        public static string FormatString(object objInput)
        {
            //To Do
            return objInput.ToString();
        }
        public static bool FormatBool(object objInput)
        {
            bool blnResult = false;
            bool.TryParse(objInput.ToString(), out blnResult);
            return blnResult;
        }
        public static int FormatInt(object objInput)
        {
            int intReturn = 0;
            int.TryParse(objInput.ToString(), out intReturn);
            return intReturn;
        }
        public static DateTime FormatDateTime(object objInput)
        { 
            DateTime dtReturn = DateTime.Now;
            DateTime.TryParse(objInput.ToString(), out dtReturn);
            return dtReturn;
        }
        public static void DeleteOldFiles()
        {
            List<string> lstPaths = new List<string>();
            lstPaths.Add(@"E:\temp\DEL");
            TimeSpan thresholdVal = new TimeSpan(0,1,0);
            foreach (var vPath in lstPaths)
            {
                var vDirectory = new DirectoryInfo(vPath);
                if (vDirectory.Exists)
                {
                    var vGetFiles = vDirectory.GetFiles().OrderBy(d => d.LastWriteTime);
                    //Leave the recent file and remove every thing.
                    foreach (var vItem in vGetFiles)
                    {
                        bool bln = Convert.ToInt64(File.GetLastWriteTime(vItem.FullName).ToString("ddMMyyyyHHmmss")) < Convert.ToInt64(DateTime.Now.AddMinutes(-5).ToString("ddMMyyyyHHmmss"));
                        if (bln)
                        {
                            File.Delete(vItem.FullName);
                        }
                    }
                }
            }
        }
    }
}
