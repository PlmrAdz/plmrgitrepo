﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
//using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using PLMREntities;
using PlmrsBal;
using System.Threading.Tasks;
using System.Text;
using System.IO;

namespace PLMRWebApi.Controllers
{
    public class PlmrController : Controller
    {

        private ITimeTrackerLayer balHelper;
        //
        // GET: /Plmr/
        // GET api/values
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }

       [System.Web.Http.HttpGet]
        public string Test()
        {
            return "Hello";
        }

        private Dictionary<string,string> RetrieveSettings(string strSessionKey)
        {
            //To Do validate sessionkey
            Dictionary<string, string> lstSettings = new Dictionary<string, string>();
            lstSettings.Add("DashBoard", "1");
            lstSettings.Add("Reports", "0");
            lstSettings.Add("CreateTask", "0");
            return lstSettings;
        }
       [System.Web.Http.HttpPost]
        public JsonResult RetrieveTasks(string strSessionkey)
        {
          balHelper = new TimeTrackerBALLayer();
           if (!balHelper.ValidateSessionKey(strSessionkey))
                return Json(ErrorInformation());
            //validate and get the tasks
           int intEmpMasterID = balHelper.GetEmpMasterID(strSessionkey);
           //TT_Useractivity
           balHelper.LogUserActivity(new UserActivityEntity()
           {
               SessionKey = strSessionkey,
               EmployeeMasterID = intEmpMasterID,
               Activity = "Retrieve Tasks",
               GeoLong = "0.0",
               GeoLat = "0.0"
           });

            var vGetTasks = balHelper.GetMyTasks(strSessionkey);
            List<UserTaskEntity> lstUserTasks = new List<UserTaskEntity>();
            foreach (var vTask in vGetTasks)
            {
                lstUserTasks.Add(new UserTaskEntity()
                {
                    UserSessionKey = strSessionkey,
                    MessageType = "Tasks",
                    TaskID = vTask.TaskID,
                    TaskDescription =vTask.TaskDescription,
                    Remarks = vTask.Remarks,
                    StatusID = vTask.StatusID,
                    StatusName=vTask.StatusName
                });
            }


            List<UserPayLoadEntity> lstReturn = new List<UserPayLoadEntity>();
            //Send UserPayLoad
            UserPayLoadEntity usrReturn = new UserPayLoadEntity()
            {
                MessageType = "Tasks",
                UserSessionKey = strSessionkey,
                Settings = RetrieveSettings(strSessionkey),
                UserTasks = lstUserTasks,
            };
            lstReturn.Add(usrReturn);

         return  Json(lstReturn);
        }

        [System.Web.Http.HttpPost]
        public JsonResult ValidateCustomer(LoginPayLoadEntity custLoad)
        {
            ConfirmationDataEntity cData = null;
            balHelper = new TimeTrackerBALLayer();
            //Validate Loginpayload-signIn-SignOut            
            if (custLoad != null)
            {
                if ((custLoad.MessageType.Equals("SignIn",StringComparison.OrdinalIgnoreCase)) || (custLoad.MessageType.Equals("SignOut",StringComparison.OrdinalIgnoreCase)))
                {
                    var strSessionKey = balHelper.Validateuser(custLoad);
                    if (!string.IsNullOrWhiteSpace(strSessionKey))
                    {
                        balHelper.LogAttendence(strSessionKey, custLoad); 
                        
                        int intEmpMasterID = balHelper.GetEmpMasterID(strSessionKey);
                        //Log TT_UserActivity
                        balHelper.LogUserActivity(new UserActivityEntity()
                        {
                            SessionKey = strSessionKey,
                            EmployeeMasterID = intEmpMasterID,
                            Activity = custLoad.MessageType,
                            GeoLong = custLoad.GeoLong,
                            GeoLat = custLoad.GeoLat
                        });
                        cData= new ConfirmationDataEntity() { Message = "success", CommandAvailable = "RetrieveTasks", SessionKey = strSessionKey };
                    }
                    else
                        cData =ErrorInformation();
                }
                else
                    cData =ErrorInformation();
            }
            else
                cData = ErrorInformation();


            return Json(cData);
        }

        [System.Web.Http.HttpPost]
        public JsonResult PingGeoData(HeartBeatEntity pingData)
        {
            ConfirmationDataEntity cData = null;
            balHelper = new TimeTrackerBALLayer();
            //validate session key
            if (!ValidateSessionKey(pingData.UserSessionKey))
                return Json(ErrorInformation());

            int intEmpMasterID = balHelper.GetEmpMasterID(pingData.UserSessionKey);
           
            balHelper.LogUserActivity(new UserActivityEntity()
            {
                SessionKey = pingData.UserSessionKey,
                EmployeeMasterID = intEmpMasterID,
                Activity = "Ping Data",
                GeoLong = pingData.GeoLong,
                GeoLat = pingData.GeoLat
            });
            //if any tasks then send to push for user task request.else make CommandAvailable = string.empty
            cData = new ConfirmationDataEntity() { Message = "Success", CommandAvailable = "Ping",SessionKey=pingData.UserSessionKey };

            return Json(cData);
        }

        private ConfirmationDataEntity ErrorInformation()
        {
            return new ConfirmationDataEntity() { Message = "Error", CommandAvailable = "Retry" };
        }

        [System.Web.Http.HttpPost]
        public JsonResult UpdateTasks(UserTaskEntity usertasks)
        {
            ConfirmationDataEntity cData = null;
            balHelper = new TimeTrackerBALLayer();
            //Validate usersession key
            //Insert or select or update task into DB based on message type
            //Send Confirmation - TokenMessage 
            if (!ValidateSessionKey(usertasks.UserSessionKey))
                return Json(ErrorInformation());
            else
            {
                int intEmpMasterID = balHelper.GetEmpMasterID(usertasks.UserSessionKey);
                balHelper.LogUserActivity(new UserActivityEntity()
                {
                    SessionKey = usertasks.UserSessionKey,
                    EmployeeMasterID = intEmpMasterID,
                    Activity = string.Format("Update Task {0}",usertasks.TaskID),
                    GeoLong = usertasks.GeoLong,
                    GeoLat = usertasks.GeoLat,
                    ReferenceID = usertasks.TaskID
                });
                balHelper.UpdateTask(usertasks);             
            }
            cData = new ConfirmationDataEntity() { Message = "Success", CommandAvailable = "PingGeofencedata",SessionKey=usertasks.UserSessionKey };
            return Json(cData);
        }

        private bool ValidateSessionKey(string strKey)
        {
            BalLayer blHelper = new BalLayer();//To do using
            return blHelper.ValidateSessionKey(strKey);
        }
    }
    public class UploadController : ApiController
    {
        public HttpResponseMessage Test()
        {
            return new HttpResponseMessage(HttpStatusCode.Accepted);
        }
        public async Task<HttpResponseMessage> PostFile()
        {
            // Check if the request contains multipart/form-data.
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            string root = HttpContext.Current.Server.MapPath("~/App_Data");
            var provider = new MultipartFormDataStreamProvider(root);

            try
            {
                StringBuilder sb = new StringBuilder(); // Holds the response body

                // Read the form data and return an async task.
                await Request.Content.ReadAsMultipartAsync(provider);

                // This illustrates how to get the form data.
                foreach (var key in provider.FormData.AllKeys)
                {
                    foreach (var val in provider.FormData.GetValues(key))
                    {
                        sb.Append(string.Format("{0}: {1}\n", key, val));
                    }
                }

                // This illustrates how to get the file names for uploaded files.
                foreach (var file in provider.FileData)
                {
                    FileInfo fileInfo = new FileInfo(file.LocalFileName);
                    sb.Append(string.Format("Uploaded file: {0} ({1} bytes)\n", fileInfo.Name, fileInfo.Length));
                }
                return new HttpResponseMessage()
                {
                    Content = new StringContent(sb.ToString())
                };
            }
            catch (System.Exception e)
            {
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }
        }

    }
}
