﻿using System.Web;
using System.Web.Mvc;

namespace PLMRWebApi
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new ValidateModelStateAttribute());//To Do check here for the call.
        }
    }
}