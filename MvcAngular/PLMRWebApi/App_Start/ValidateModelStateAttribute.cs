﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Web.Http.ModelBinding;

namespace PLMRWebApi
{
    public class ValidateModelStateAttribute : System.Web.Http.Filters.ActionFilterAttribute, IActionFilter
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            if (!actionContext.ModelState.IsValid)
            {
                string errMessage = string.Empty;
                foreach (var modelState in actionContext.ModelState.Values)
                {
                    foreach (var error in modelState.Errors)
                    {
                        errMessage += "Error Message : " + error.ErrorMessage;
                    }
                }
                actionContext.Response = new System.Net.Http.HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }
       
    }
}