﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//using BarcodeLib;
namespace BarCodeGenerator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            BarcodeLib.Barcode b = new BarcodeLib.Barcode();
            BarcodeLib.TYPE type = BarcodeLib.TYPE.CODE39;
            b.Alignment = BarcodeLib.AlignmentPositions.CENTER;
            //b.BarWidth = textBoxBarWidth.Text.Trim().Length < 1 ? null : (int?)Convert.ToInt32(textBoxBarWidth.Text.Trim());
            //b.RotateFlipType = (RotateFlipType)Enum.Parse(typeof(RotateFlipType), this.cbRotateFlip.SelectedItem.ToString(), true);
            //b.BarWidth = 300;
            b.Width = 300;
            b.Height = 200;
            b.LabelPosition = BarcodeLib.LabelPositions.BOTTOMCENTER;
            b.IncludeLabel = true;
            b.AlternateLabel="GUEST";
            Image imgEncoded = b.Encode(type, "PLMR", Color.Black, Color.White, 100, 50);
            //barcode.Location = new Point((this.barcode.Location.X + this.barcode.Width / 2) - barcode.Width / 2, (this.barcode.Location.Y + this.barcode.Height / 2) - barcode.Height / 2);            
            b.SaveImage(@"e:\temp\2.png", BarcodeLib.SaveTypes.BMP);
            //.SaveImage(sfd.FileName, savetype);
        }
    }
}
