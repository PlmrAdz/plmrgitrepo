﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Gma.QrCodeNet.Encoding;
using System.IO;
namespace QRCodeGenerator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnQRCode_Click(object sender, EventArgs e)
        {
            //20W
            string strProductCode = cmbDrivers.Text;

            string strTitle = string.Empty;
            for (int i = 100; i < 110; i++)
            {
                strTitle =string.Format("http://plmrlightingsolutions.com/ProductInfo.html?PID={0}{1}",strProductCode,i);
                using (Bitmap bmp = GenerateQRCode(strTitle, Color.Black, Color.White))
                {
                    bmp.Save(string.Format(@"E:\temp\{0}{1}.png",strProductCode,i));
                }
            }
            MessageBox.Show("completed");
        }
        private Bitmap GenerateQRCode(string text, System.Drawing.Color DarkColor, System.Drawing.Color LightColor)
        {
            QrEncoder Encoder = new QrEncoder(Gma.QrCodeNet.Encoding.ErrorCorrectionLevel.H);
            QrCode Code = Encoder.Encode(text);
            Bitmap TempBMP = new Bitmap(Code.Matrix.Width, Code.Matrix.Height);
            for (int X = 0; X <= Code.Matrix.Width - 1; X++)
            {
                for (int Y = 0; Y <= Code.Matrix.Height - 1; Y++)
                {
                    if (Code.Matrix.InternalArray[X, Y])
                        TempBMP.SetPixel(X, Y, DarkColor);
                    else
                        TempBMP.SetPixel(X, Y, LightColor);
                }
            }
            return TempBMP;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            var vGetList = Enum.GetValues(typeof(LEDDriver));
            foreach (var vItem in vGetList)
            {
                cmbDrivers.Items.Add(vItem);
            }
            
        }
        private BarcodeLib.Barcode b;
        private void btnBar_Click(object sender, EventArgs e)
        {
            b = new BarcodeLib.Barcode();
            BarcodeLib.TYPE type = BarcodeLib.TYPE.CODE39;
            b.Alignment = BarcodeLib.AlignmentPositions.CENTER;
            //b.BarWidth = textBoxBarWidth.Text.Trim().Length < 1 ? null : (int?)Convert.ToInt32(textBoxBarWidth.Text.Trim());
            //b.RotateFlipType = (RotateFlipType)Enum.Parse(typeof(RotateFlipType), this.cbRotateFlip.SelectedItem.ToString(), true);
            //b.BarWidth = 300;
            b.Width = 300;
            b.Height = 200;
            b.LabelPosition = BarcodeLib.LabelPositions.BOTTOMCENTER;
            b.IncludeLabel = true;
            b.AlternateLabel = "GUEST";
            Image imgEncoded = b.Encode(type, "PLMR", Color.Black, Color.White, 100, 50);
            //barcode.Location = new Point((this.barcode.Location.X + this.barcode.Width / 2) - barcode.Width / 2, (this.barcode.Location.Y + this.barcode.Height / 2) - barcode.Height / 2);            
            b.SaveImage(@"e:\temp\2.png", BarcodeLib.SaveTypes.BMP);
            //.SaveImage(sfd.FileName, savetype);
        }

        private void btnQrBar_Click(object sender, EventArgs e)
        {
            string strProductCode = cmbDrivers.Text;
            if(string.IsNullOrWhiteSpace(strProductCode))
                return;

            string strTitle = string.Empty;
            string strFolderName = @"e:\temp\30W\";
            if(!Directory.Exists(strFolderName))
                Directory.CreateDirectory(strFolderName);

            for (int i = 100; i < 110; i++)
            {
                //QR code
                strTitle = string.Format("http://plmrlightingsolutions.com/ProductInfo.html?PID={0}{1}", strProductCode, i);
                using (Bitmap bmp = GenerateQRCode(strTitle, Color.Black, Color.White))
                {
                    bmp.Save(string.Format(@"{0}{1}{2}.png",strFolderName,strProductCode, i));
                }
                
                //bar code
                using (b = new BarcodeLib.Barcode())
                {
                    BarcodeLib.TYPE type = BarcodeLib.TYPE.CODE39;
                    b.Alignment = BarcodeLib.AlignmentPositions.CENTER;
                    //b.Width = 500;
                    b.Height = 200;
                    b.LabelPosition = BarcodeLib.LabelPositions.BOTTOMCENTER;
                    b.IncludeLabel = true;
                    //b.AlternateLabel = "GUEST";
                    b.BarWidth = 2;
                    Image imgEncoded = b.Encode(type, string.Format("{0}{1}",strProductCode, i), Color.Black, Color.White, 100, 50);
                    b.SaveImage(string.Format("{0}{1}{2}.jpg", strFolderName ,strProductCode, i), BarcodeLib.SaveTypes.JPG);
                }
            }
            MessageBox.Show("completed");            
        }
    }
    public enum LEDDriver
    { 
        DCD202017,
        DCD302017,
        DCD502017,
        DCD1002017,        
    }
}
