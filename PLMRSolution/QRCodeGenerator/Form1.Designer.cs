﻿namespace QRCodeGenerator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnQRCode = new System.Windows.Forms.Button();
            this.cmbDrivers = new System.Windows.Forms.ComboBox();
            this.btnBar = new System.Windows.Forms.Button();
            this.btnQrBar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnQRCode
            // 
            this.btnQRCode.Location = new System.Drawing.Point(171, 70);
            this.btnQRCode.Name = "btnQRCode";
            this.btnQRCode.Size = new System.Drawing.Size(98, 23);
            this.btnQRCode.TabIndex = 0;
            this.btnQRCode.Text = "Generate QR";
            this.btnQRCode.UseVisualStyleBackColor = true;
            this.btnQRCode.Click += new System.EventHandler(this.btnQRCode_Click);
            // 
            // cmbDrivers
            // 
            this.cmbDrivers.FormattingEnabled = true;
            this.cmbDrivers.Location = new System.Drawing.Point(148, 34);
            this.cmbDrivers.Name = "cmbDrivers";
            this.cmbDrivers.Size = new System.Drawing.Size(121, 21);
            this.cmbDrivers.TabIndex = 1;
            // 
            // btnBar
            // 
            this.btnBar.Location = new System.Drawing.Point(171, 110);
            this.btnBar.Name = "btnBar";
            this.btnBar.Size = new System.Drawing.Size(98, 23);
            this.btnBar.TabIndex = 2;
            this.btnBar.Text = "Generate Bar";
            this.btnBar.UseVisualStyleBackColor = true;
            this.btnBar.Click += new System.EventHandler(this.btnBar_Click);
            // 
            // btnQrBar
            // 
            this.btnQrBar.Location = new System.Drawing.Point(275, 32);
            this.btnQrBar.Name = "btnQrBar";
            this.btnQrBar.Size = new System.Drawing.Size(98, 23);
            this.btnQrBar.TabIndex = 3;
            this.btnQrBar.Text = "QR - BAR";
            this.btnQrBar.UseVisualStyleBackColor = true;
            this.btnQrBar.Click += new System.EventHandler(this.btnQrBar_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(691, 317);
            this.Controls.Add(this.btnQrBar);
            this.Controls.Add(this.btnBar);
            this.Controls.Add(this.cmbDrivers);
            this.Controls.Add(this.btnQRCode);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnQRCode;
        private System.Windows.Forms.ComboBox cmbDrivers;
        private System.Windows.Forms.Button btnBar;
        private System.Windows.Forms.Button btnQrBar;
    }
}

